---
marp: true
theme: gaia
_class: lead
paginate: true
backgroundColor: #fff
backgroundImage: url('https://marp.app/assets/hero-background.svg')
---

# **Headers Exchange💱**

---

# Characteristics

* Messages are based on arguments containing headers and optional values.

* Unlike topic exchange route messages are based on header values instead of routing keys.

* A message matches if the value of the header equals the value specified upon binding.

---

# How headers exchange works

* Special argument named "x-match" ("any", "all").
* Headers could be number, string or  object.


---

![bg fit](images/simple-exchage.png)

---

![bg fit](images/headers-exchange.png)

---

![bg fit](images/stats1.png)

---

![bg fit](images/stats2.png)
