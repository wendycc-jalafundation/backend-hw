let buffer = new ArrayBuffer(16);
if (buffer.byteLength === 16) {
  console.log("Yes, it's 16 bytes.");
} else {
  console.log("Oh no, it's the wrong size!");
}

let int32View = new Int32Array(buffer);
for (let i = 0; i < int32View.length; i++) {
    int32View[i] = i * 2;
  }

  //Int8Array
  //Uint8Array
  //Uint8ClampedArray
  //Int16Array
  //Uint16Array
  //Int32Array
  //Uint32Array
  //Float32Array