import { assert } from "chai";

// Declare an array
let myNewArray = [ "Jack", "Sawyer", "John", "Desmond" ];

// Empty array
let myEmpty = [];

//using new Array()
let lostArray = new Array("Jack", "Sawyer", "John", "Desmond" );

// Accessing elements
let myArray = ["Jack", "Sawyer", "John", "Desmond"];

assert.equal(myArray[0],"Jack"); //Jack is expected

//Multidimensional arrays
let familyArray = ["Marge", "Homer", ["Bart", "Lisa", "Maggie"]];
assert.equal(familyArray[2][1], "Lisa") //Lisa is expected

//Adding, removing items
// Adding using index
myArray = [ "Kate", "Sun"];
myArray[2] = "Juliet";

// Adding items at the end using push() 
myArray = [ "Kate", "Sun"];
myArray.push("Juliet"); // Adds "Juliet" at the end of the array
myArray.push("Libby", "Shannon");// Adds "Libby" and "Shannon" at the end of the array.

// Adding items at the beginning using unshift()
myArray = [ "Kate", "Sun"];
myArray.unshift("Juliet"); // Adds "Juliet" at the beginning of the array
myArray.unshift("Libby", "Shannon");// Adds "Libby" and "Shannon" at the beginning of the array

//Removing the first and the last element using pop() and shift()
myArray = ["Jack", "Sawyer", "John", "Desmond", "Kate"];
myArray.pop(); // Removes "Kate"
myArray.shift(); // Removes "Jack"

// find() and filter()
let animals = [
    {name: 'Tibbers', type: 'cat', isNeutered: true, age: 2},
    {name: 'Fluffball', type: 'rabbit', isNeutered: false, age: 1},
    {name: 'Strawhat', type: 'cat', isNeutered: true, age: 5}
  ]

animalTypeFound = animals.find( animal => animal.type === 'cat' );

// animalTypeFound will return:
// {name: 'Tibbers', type: 'cat', isNeutered: true, age: 2}

animalTypeFilter = animals.filter( animal => animal.type === 'cat' );

// animalTypeFilter will return:
// [{name: 'Tibbers', type: 'cat', isNeutered: true, age: 2}, {name: 'Strawhat', type: 'cat', isNeutered: true, age: 5}]


// map()
let animals = [
    {name: 'Tibbers', type: 'cat', isNeutered: true, age: 2},
    {name: 'Fluffball', type: 'rabbit', isNeutered: false, age: 1},
    {name: 'Strawhat', type: 'cat', isNeutered: true, age: 5}
  ]

// get the animals' names: 
// ['Tibbers', 'Fluffball', 'Strawhat']

let animalNames = animals.map(animal => {return animal.name});

// get the name and species
// [{name: 'Tibbers', species: 'cat'}, {name: 'Fluffball', species: 'rabbit'}, {name: 'Strawhat', species: 'cat'}]

let petDetails = animals.map(animal => {
    return {
        name: animal.name, 
        species: animal.type
    };
});

// flatMap()

const numbersList = ['one', 'two', 'three', 'four'];
const thingsList = ['ball', 'laptops', 'books', 'letters'];

const mappedList = numbersList.map((numberItem, index) => [numberItem, thingsList[index]]);
console.log(mappedList);
// [['one', 'ball'], ['two', 'laptops'], ['three', 'books'], ['four', 'letters']

const mappedAndFlattenList = numbersList.flatMap((numberItem, index) => [numberItem, thingsList[index]]);
console.log(mappedAndFlattenList);
// ['one', 'ball', 'two', 'laptops', 'three', 'books', 'four', 'letters']


