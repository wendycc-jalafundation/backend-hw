# Arrays
## Examples
Link to all [the examples](/collectionHomework)
## Basic array operations
```js 
// Declare an array
let myArray = [ "Jack", "Sawyer", "John", "Desmond" ];

// Empty array
let myArray = [];

//using new Array()
let lostArray = new Array("Jack", "Sawyer", "John", "Desmond" );

// Accessing elements
var myArray = ["Jack", "Sawyer", "John", "Desmond"];

console.log(myArray[0], "Jack"); //Prints "Jack"
ass
```
## 
## for-f and arrays - ES6
for..of is a method, introduced in ES2015, for iterating over "iterable collections". These are objects that have a [Symbol.iterator] property.
```js
const array = ['a', 'b', 'c', 'd'];
for (const item of array) {
	console.log(item)
}
// Result: a, b, c, d

const string = 'Ire Aderinokun';
for (const character of string) {
	console.log(character)
}
// Result: I, r, e, , A, d, e, r, i, n, o, k, u, n
```
## Multidimensional Arrays
```js
//Multidimensional arrays
let familyArray = ["Marge", "Homer", ["Bart", "Lisa", "Maggie"]];
assert.equal(familyArray[2][1], "Lisa") //Lisa is expected
```
## Adding and removing elements
```js
//Adding, removing items
// Adding using index
myArray = [ "Kate", "Sun"];
myArray[2] = "Juliet";

// Adding items at the end using push() 
myArray = [ "Kate", "Sun"];
myArray.push("Juliet"); // Adds "Juliet" at the end of the array
myArray.push("Libby", "Shannon");// Adds "Libby" and "Shannon" at the end of the array.

// Adding items at the beginning using unshift()
myArray = [ "Kate", "Sun"];
myArray.unshift("Juliet"); // Adds "Juliet" at the beginning of the array
myArray.unshift("Libby", "Shannon");// Adds "Libby" and "Shannon" at the beginning of the array

//Removing the first and the last element using pop() and shift()
myArray = ["Jack", "Sawyer", "John", "Desmond", "Kate"];
myArray.pop(); // Removes "Kate"
myArray.shift(); // Removes "Jack"
```
## .find(), .map(), .filter(), .flatMap(), .sort(), etc
1. `map` returns an array with the same length,
2. `filter` as the name implies, it returns an array with less items than the original array
3. `reduce` returns a single value (or object)
4. `find` returns the first item in an array that satisfies a condition

```js
// find() and filter()
let animals = [
    {name: 'Tibbers', type: 'cat', isNeutered: true, age: 2},
    {name: 'Fluffball', type: 'rabbit', isNeutered: false, age: 1},
    {name: 'Strawhat', type: 'cat', isNeutered: true, age: 5}
  ]

animalTypeFound = animals.find( animal => animal.type === 'cat' );

// animalTypeFound will return:
// {name: 'Tibbers', type: 'cat', isNeutered: true, age: 2}

animalTypeFilter = animals.filter( animal => animal.type === 'cat' );

// animalTypeFilter will return:
// [{name: 'Tibbers', type: 'cat', isNeutered: true, age: 2}, {name: 'Strawhat', type: 'cat', isNeutered: true, age: 5}]


// map()
let animals = [
    {name: 'Tibbers', type: 'cat', isNeutered: true, age: 2},
    {name: 'Fluffball', type: 'rabbit', isNeutered: false, age: 1},
    {name: 'Strawhat', type: 'cat', isNeutered: true, age: 5}
  ]

// get the animals' names: 
// ['Tibbers', 'Fluffball', 'Strawhat']

let animalNames = animals.map(animal => {return animal.name});

// get the name and species
// [{name: 'Tibbers', species: 'cat'}, {name: 'Fluffball', species: 'rabbit'}, {name: 'Strawhat', species: 'cat'}]

let petDetails = animals.map(animal => {
    return {
        name: animal.name, 
        species: animal.type
    };
});

// flatMap()

const numbersList = ['one', 'two', 'three', 'four'];
const thingsList = ['ball', 'laptops', 'books', 'letters'];

const mappedList = numbersList.map((numberItem, index) => [numberItem, thingsList[index]]);
console.log(mappedList);
// [['one', 'ball'], ['two', 'laptops'], ['three', 'books'], ['four', 'letters']

const mappedAndFlattenList = numbersList.flatMap((numberItem, index) => [numberItem, thingsList[index]]);
console.log(mappedAndFlattenList);
// ['one', 'ball', 'two', 'laptops', 'three', 'books', 'four', 'letters']

```

# Typed Arrays
## Example
```js
let buffer = new ArrayBuffer(16);
if (buffer.byteLength === 16) {
  console.log("Yes, it's 16 bytes.");
} else {
  console.log("Oh no, it's the wrong size!");
}
```
# Maps
## Defintion
Map is a collection of keyed data items, just like an Object. But the main difference is that Map allows keys of any type.

Methods:
- new Map() -- creates the map.
- map.set(key, value) -- stores the value by the key.
- map.get(key) -- returns the value by the key, undefined if key doesn't exist in map.
- map.has(key) -- returns true if the key exists, false otherwise.
- map.delete(key) -- removes the value by the key.
- map.clear() -- clears the map
- map.size -- returns the current element count.
## Example
```js
let map = new Map();

map.set('1', 'str1');   // a string key
map.set(1, 'num1');     // a numeric key
map.set(true, 'bool1'); // a boolean key

// remember the regular Object? it would convert keys to string
// Map keeps the type, so these two are different:
alert( map.get(1)   ); // 'num1'
alert( map.get('1') ); // 'str1'

alert( map.size ); // 3

let john = { name: "John" };

// for every user, let's store their visits count
let visitsCountMap = new Map();

// john is the key for the map
visitsCountMap.set(john, 123);

console.log( visitsCountMap.get(john) );

//looping over a map
map.keys() 
map.values() 
map.entries()
```
# Weak Map
## Definition
The first difference between Map and WeakMap is that keys must be objects, not primitive values:
If we use an object as the key in it, and there are no other references to that object – it will be removed from memory (and from the map) automatically
Methods:

- weakMap.get(key)
- weakMap.set(key, value)
- weakMap.delete(key)
- weakMap.has(key)
## Example
```js
let weakMap = new WeakMap();

let obj = {};

weakMap.set(obj, "ok"); // works fine (object key)

// can't use a string as the key
weakMap.set("test", "Whoops"); // Error, because "test" is not an object

let john = { name: "John" };

let weakMap = new WeakMap();
weakMap.set(john, "...");

john = null; // overwrite the reference

// john is removed from memory!
```
# Set
## Definition
The Set object lets you store unique values of any type, whether primitive values or object references.

Methods:
- new Set(iterable)
- set.add(value) 
- set.delete(value)
- set.has(value)
- set.clear()
- set.size 

## Example

```js
let set = new Set();

let john = { name: "John" };
let pete = { name: "Pete" };
let mary = { name: "Mary" };

// visits, some users come multiple times
set.add(john);
set.add(pete);
set.add(mary);
set.add(john);
set.add(mary);

// set keeps only unique values
console.log( set.size ); // 3

for (let user of set) {
  console.log(user.name); // John (then Pete and Mary)
}
```
# Weak Sets
## Definition
It is analogous to Set, but we may only add objects to WeakSet (not primitives).

An object exists in the set while it is reachable from somewhere else.
## Example
```js
let visitedSet = new WeakSet();

let john = { name: "John" };
let pete = { name: "Pete" };
let mary = { name: "Mary" };

visitedSet.add(john); // John visited us
visitedSet.add(pete); // Then Pete
visitedSet.add(john); // John again

// visitedSet has 2 users now

// check if John visited?
console.log(visitedSet.has(john)); // true

// check if Mary visited?
console.log(visitedSet.has(mary)); // false

john = null;

// visitedSet will be cleaned automatically
```
# Synchronous generators
## what are synchronous generators?

## yield
## Example



