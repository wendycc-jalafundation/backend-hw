import { assert } from "chai"
import {getAvailableDoctors} from "../university.mjs"

it ('Testing', async function(){
    
    const time = new Date().getHours();
    let availableDoctors = await getAvailableDoctors();
    let system = availableDoctors.system;

    if ((time > 19 && time < 23) || (time > 0 && time < 7)) {
        assert.equal(system, 'San Pedro', 'The doctors listed belong to San Pedro');
    }
    else {
        assert.equal(system, 'Los Olivos', 'The doctors listed belong to Los Olivos');
    }
})

