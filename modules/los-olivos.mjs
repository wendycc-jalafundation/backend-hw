const doctors = {
    system: 'Los Olivos',
    doctors: ['Los', 'Olivos']
};

function getDoctors() {
    return doctors;
}
export { getDoctors }
