const doctors = {
    system: 'San Pedro',
    doctors: ['Juan', 'Pedro']
};

function getDoctors() {
    return doctors;
}
export { getDoctors }