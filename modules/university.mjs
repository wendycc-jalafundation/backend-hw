
async function getAvailableDoctors()
{
    const time= new Date().getHours();
    let availableDoctors;
    
    if ((time > 19 && time <23) || (time > 0 && time <7) )
    {
        let m = await import ('./san-pedro.mjs');
        availableDoctors= m.getDoctors();
    }
    else{
        let m = await import ('./los-olivos.mjs');
        availableDoctors= m.getDoctors();
    }

    return availableDoctors;
}
let ad= await getAvailableDoctors();

console.log(ad.system);
export {getAvailableDoctors}


