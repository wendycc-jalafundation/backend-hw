import * as mongoDB from "mongodb";
import * as dotenv from "dotenv";

export const collections: { attendance?: mongoDB.Collection } = {};


export async function connectToDatabase() {
  dotenv.config();

  const client: mongoDB.MongoClient = new mongoDB.MongoClient(
    "mongodb://localhost:27017/"
  );

  await client.connect();

  const db: mongoDB.Db = client.db("attendance");

  const attendanceCollection: mongoDB.Collection = db.collection("attendance");

  collections.attendance = attendanceCollection;

  console.log(
    `Successfully connected to database: ${db.databaseName} and collection: ${attendanceCollection.collectionName}`
  );
}
