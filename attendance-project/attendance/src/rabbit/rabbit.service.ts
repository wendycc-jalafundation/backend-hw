import { Options, Connection, Channel } from "amqplib";
import amqp = require("amqplib");
import { AttendanceRepository } from "../attendance/attendance.repository";
import AttendaceService from "../attendance/attendance.service";

export default class RabbitService {
  repository = new AttendanceRepository();
  connection!: Connection;
  channel!: Channel;
  constructor() {}
  async connect(): Promise<void> {
    const options: Options.Connect = {
      protocol: "amqp",
      hostname: "localhost",
      port: 5672,
      username: "admin",
      password: "admin",
    };
    try {
      this.connection = await amqp.connect(options);
      this.channel = await this.connection.createChannel();
      await this.channel.assertQueue("attendance", { durable: true });
      console.log("Rabbit is waiting for messages...");
      await this.listenforUsersQueue()
    } catch (e) {
      console.log("Error connecting to Rabbit");
    }
  }
  sendMessage(message: any) {
    if (message) {
      this.channel.sendToQueue("attendance", Buffer.from(message));
      console.log(`Sending message: ${message}`);
      console.log("Rabbit is waiting for messages...");
    }
  }
  async listenforUsersQueue() {
    const attendance = this.repository;
    await this.channel.consume("users", async function (message) {
      console.log(`Message received User ID: ${message?.content}`);
      const user = message?.content.toString() as string;
      const messageObj = user.split("_");
      const userId = messageObj[1];
      // console.log(messageObj);
      if (messageObj) {
        const deleteAttendanceByUser = await attendance.deleteByUserId(userId)
        console.log(`Rabbit: ${deleteAttendanceByUser?.deletedCount} attendances were deleted for user ${messageObj[1]}`);
      }
    });
  }
}
