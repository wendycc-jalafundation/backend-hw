export default interface Attendance{
    startTime: string;
    endTime: string;
    date: Date;
    notes: string;
    userId: string;
}