import { collections } from "../db/database.service";
import Attendance from "./attendance.model";
import { ObjectId } from 'bson';
export class AttendanceRepository {
  async findAll() {
    return await collections.attendance?.find({}).toArray();
  }

  async findByUserId(id: string) {
    return await collections.attendance?.find({ userId: id }).toArray();
  }
  async create(attendance: Attendance) {
    return await collections.attendance?.insertOne(attendance);
  }

  async delete(id: string) {
    return await collections.attendance?.deleteOne({ _id: new ObjectId(id) });
  }

  async findById(id: string) {
      return await collections.attendance?.findOne({ _id: new ObjectId(id) });
  }
  async deleteByUserId(userId: string){
    return await collections.attendance?.deleteMany({
      userId:userId
    })
  }
}
