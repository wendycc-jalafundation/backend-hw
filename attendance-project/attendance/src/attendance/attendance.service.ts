import RabbitService from "../rabbit/rabbit.service";
import Attendance from "./attendance.model";
import { AttendanceRepository } from "./attendance.repository";

export default class AttendaceService {
  repository = new AttendanceRepository();

  constructor(private rabbit : RabbitService) {}

  async list() {
    return await this.repository.findAll();
  }

  async listByUser(id: string) {
    return await this.repository.findByUserId(id);
  }

  async create(data: any) {
    let regexHoursMinutes: RegExp = /^(10|11|12|[1-9]):[0-5][0-9]$/;
    const isValidStartTime = regexHoursMinutes.test(data.startTime);
    const isValidEndTime = regexHoursMinutes.test(data.endTime);
    const isValidDate = new Date(data.date);

    if (
      !isValidEndTime ||
      !isValidStartTime ||
      isValidDate.toString() == "Invalid Date"
    ) {
      return await {
        code: 400,
        message: "Incorrect date time formats",
        data: {},
      };
    }

    const attendance: Attendance = {
      startTime: data.startTime,
      endTime: data.endTime,
      date: new Date(data.date),
      notes: data.notes,
      userId: data.userId,
    };
    const result = await this.repository.create(attendance);

    this.rabbit.sendMessage(`update_${attendance.userId}`);

    return await {
      code: 200,
      message: "Attendance created",
      data: {
        _id: result?.insertedId,
        ...attendance,
      },
    };
  }

  async delete(id: string, userId: string) {
    console.log("service");

    const attendance = await this.repository.findById(id);
    if (attendance?.userId != userId)
      return await {
        code: 400,
        message: "Incorrect user id or attendance does not exist",
        data: {},
      };

    const deleteResponse = await this.repository.delete(id);
    console.log(deleteResponse);
    if (deleteResponse?.acknowledged){
      this.rabbit.sendMessage(`delete_${attendance.userId}`);
      return await {
        code: 200,
        message: "Attendance deleted",
        data: {},
      };
    }
    

    return await {
      code: 400,
      message: "Incorrect user id or attendance does not exist",
      data: {},
    };
  }

  async deleteAllByUserId(userId: string){
    
    const deleteResponse = await this.repository.deleteByUserId(userId);
    console.log(deleteResponse);
    return deleteResponse;
    
  }
}
