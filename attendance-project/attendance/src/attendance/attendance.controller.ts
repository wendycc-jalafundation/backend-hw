import express, { Request, Response } from "express";
import RabbitService from "../rabbit/rabbit.service";
import AttendaceService from "./attendance.service";
export default class AttendanceController {
  path = "/attendance";
  router = express.Router();
  constructor(private attendanceService: AttendaceService) {
    this.router.get(this.path, this.list.bind(this));
    this.router.get(this.path + "/user/:id", this.listByUser.bind(this));
    this.router.post(this.path, this.create.bind(this));
    this.router.delete(this.path, this.delete.bind(this));


  }
  async list(_req: Request, res: Response) {
    try {
      const result = await this.attendanceService.list();
      res.status(200).json(result);
    } catch (e) {
      res.status(500);
    }
  }

  async listByUser(_req: Request, res: Response) {
    try {
      const userId = _req.params.id;
      const result = await this.attendanceService.listByUser(userId);
      res.status(200).json(result);
    } catch (e) {
      res.status(500);
    }
  }
  async create(_req: Request, res: Response) {
    try {
      const result = await this.attendanceService.create(_req.body);
      
      res.status(200).json(result);
    } catch (e) {
      res.status(500);
    }
  }
  async delete(_req: Request, res: Response) {
    console.log('controller');
    
    try {
      const attId = _req.query.id as string;
      const userId = _req.query.userid as string
      const result = await this.attendanceService.delete(attId, userId);
      res.status(200).json(result);
    } catch (e) {
      res.status(500);
    }
  }

}
