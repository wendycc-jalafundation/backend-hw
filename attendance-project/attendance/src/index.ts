import App from "./app";
import express, { Express, Request, Response } from 'express';
import dotenv from 'dotenv';
import { createConnection } from 'typeorm';
import { connectToDatabase } from "./db/database.service";
import AttendanceController from "./attendance/attendance.controller";
import RabbitService from "./rabbit/rabbit.service";
import AttendaceService from "./attendance/attendance.service";

dotenv.config();


connectToDatabase().then(()=>{
  
  const PORT = process.env.PORT || 3001;
  const rabbit = new RabbitService();
  rabbit.connect()
  const attendanceService = new AttendaceService(rabbit);
  const app = new App([new AttendanceController(attendanceService)], PORT);
  
  app.listen();
})
 

  
  






