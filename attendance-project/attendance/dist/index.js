"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var app_1 = __importDefault(require("./app"));
var dotenv_1 = __importDefault(require("dotenv"));
var database_service_1 = require("./db/database.service");
var attendance_controller_1 = __importDefault(require("./attendance/attendance.controller"));
var rabbit_service_1 = __importDefault(require("./rabbit/rabbit.service"));
var attendance_service_1 = __importDefault(require("./attendance/attendance.service"));
dotenv_1.default.config();
database_service_1.connectToDatabase().then(function () {
    var PORT = process.env.PORT || 3001;
    var rabbit = new rabbit_service_1.default();
    rabbit.connect();
    var attendanceService = new attendance_service_1.default(rabbit);
    var app = new app_1.default([new attendance_controller_1.default(attendanceService)], PORT);
    app.listen();
});
