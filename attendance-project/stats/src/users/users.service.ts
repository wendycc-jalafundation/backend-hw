import * as rm from "typed-rest-client/RestClient";

export default class UserService {
  private connection = new rm.RestClient("users", "http://localhost:3000");
  async updateUser(userid: string, totalAttendance: number) {
    const req = { attendance: totalAttendance };
    const res : rm.IRestResponse<any>  = await this.connection.replace(`/users/${userid}`, req);
    return res;
  }
}
