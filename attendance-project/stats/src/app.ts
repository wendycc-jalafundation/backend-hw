import express from "express";
import * as bodyParser from "body-parser";
import RabbitService from "./rabbit/rabbit.service";

export default class App {
  public app: express.Application;
  public port: number;
  private rabbit: RabbitService;

  constructor(controllers: any, port: any) {
    this.app = express();
    this.port = port;
    this.rabbit = new RabbitService()

    this.initializeMiddlewares();
    this.initializeControllers(controllers);
  }

  private initializeMiddlewares() {
    this.app.use(express.json());
    this.app.use(express.static("public"));
    this.app.use(bodyParser.json());
  }

  private initializeControllers(controllers: any) {
    controllers.forEach((controller: any) => {
      this.app.use("/", controller.router);
    });
  }

  public listen() {
    this.rabbit.connect()
    this.app.listen(this.port, () => {
      console.log(`Server started at http://localhost:${this.port}`);
    });
   
  }


}
