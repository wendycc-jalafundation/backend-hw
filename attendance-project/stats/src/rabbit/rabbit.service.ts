import { Options, Connection, Channel } from "amqplib";
import amqp = require("amqplib");
import AttendanceService from "../attendance/attendance.service";
import UserService from "../users/users.service";

export default class RabbitService {
  connection!: Connection;
  channel!: Channel;
  attendance = new AttendanceService();
  users = new UserService();
  constructor() {}
  async connect(): Promise<void> {
    const options: Options.Connect = {
      protocol: "amqp",
      hostname: "localhost",
      port: 5672,
      username: "admin",
      password: "admin",
    };
    try {
      this.connection = await amqp.connect(options);
      this.channel = await this.connection.createChannel();
      await this.channel.assertQueue("attendance", { durable: true });
      console.log("Rabbit is waiting for messages...");
      this.listenForMessages();
    } catch (e) {
      console.log("Error connecting to Rabbit");
    }
  }
  sendMessage(message: any) {
    if (message) {
      this.channel.sendToQueue("attendance", Buffer.from(message));
      console.log(`Sending message: ${message}`);
      console.log("Rabbit is waiting for messages...");
    }
  }

  async listenForMessages() {
    const attendance = this.attendance;
    const users = this.users;
    await this.channel.consume("attendance", async function (message) {
      console.log(`Message received User ID: ${message?.content}`);
      const user = message?.content.toString() as string;
      const messageObj = user.split("_");

      // console.log(messageObj);

      if (messageObj) {
        const attendanceResult = await attendance.attendanceByUser(
          messageObj[1]
        );
        const totalAttendance = attendanceResult.result as Object[];

        if (messageObj[0] === "update") {
          const updateUserResponse = await users.updateUser(
            messageObj[1],
            totalAttendance.length++
          );
          console.log("Rabbit: ADD", updateUserResponse.result.data);
        }

        if (messageObj[0] === "delete") {
          const deleteUserResponse = await users.updateUser(
            messageObj[1],
            totalAttendance.length--
          );
          console.log("Rabbit: DELETE", deleteUserResponse.result.data);
        }
      }
    });
  }

  
}
