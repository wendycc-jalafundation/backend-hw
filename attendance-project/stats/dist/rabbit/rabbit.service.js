"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var amqp = require("amqplib");
var attendance_service_1 = __importDefault(require("../attendance/attendance.service"));
var users_service_1 = __importDefault(require("../users/users.service"));
var RabbitService = /** @class */ (function () {
    function RabbitService() {
        this.attendance = new attendance_service_1.default();
        this.users = new users_service_1.default();
    }
    RabbitService.prototype.connect = function () {
        return __awaiter(this, void 0, void 0, function () {
            var options, _a, _b, e_1;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        options = {
                            protocol: "amqp",
                            hostname: "localhost",
                            port: 5672,
                            username: "admin",
                            password: "admin",
                        };
                        _c.label = 1;
                    case 1:
                        _c.trys.push([1, 5, , 6]);
                        _a = this;
                        return [4 /*yield*/, amqp.connect(options)];
                    case 2:
                        _a.connection = _c.sent();
                        _b = this;
                        return [4 /*yield*/, this.connection.createChannel()];
                    case 3:
                        _b.channel = _c.sent();
                        return [4 /*yield*/, this.channel.assertQueue("attendance", { durable: true })];
                    case 4:
                        _c.sent();
                        console.log("Rabbit is waiting for messages...");
                        this.listenForMessages();
                        return [3 /*break*/, 6];
                    case 5:
                        e_1 = _c.sent();
                        console.log("Error connecting to Rabbit");
                        return [3 /*break*/, 6];
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    RabbitService.prototype.sendMessage = function (message) {
        if (message) {
            this.channel.sendToQueue("attendance", Buffer.from(message));
            console.log("Sending message: " + message);
            console.log("Rabbit is waiting for messages...");
        }
    };
    RabbitService.prototype.listenForMessages = function () {
        return __awaiter(this, void 0, void 0, function () {
            var attendance, users;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        attendance = this.attendance;
                        users = this.users;
                        return [4 /*yield*/, this.channel.consume("attendance", function (message) {
                                return __awaiter(this, void 0, void 0, function () {
                                    var user, messageObj, attendanceResult, totalAttendance, updateUserResponse, deleteUserResponse;
                                    return __generator(this, function (_a) {
                                        switch (_a.label) {
                                            case 0:
                                                console.log("Message received User ID: " + (message === null || message === void 0 ? void 0 : message.content));
                                                user = message === null || message === void 0 ? void 0 : message.content.toString();
                                                messageObj = user.split("_");
                                                if (!messageObj) return [3 /*break*/, 5];
                                                return [4 /*yield*/, attendance.attendanceByUser(messageObj[1])];
                                            case 1:
                                                attendanceResult = _a.sent();
                                                totalAttendance = attendanceResult.result;
                                                if (!(messageObj[0] === "update")) return [3 /*break*/, 3];
                                                return [4 /*yield*/, users.updateUser(messageObj[1], totalAttendance.length++)];
                                            case 2:
                                                updateUserResponse = _a.sent();
                                                console.log("Rabbit: ADD", updateUserResponse.result.data);
                                                _a.label = 3;
                                            case 3:
                                                if (!(messageObj[0] === "delete")) return [3 /*break*/, 5];
                                                return [4 /*yield*/, users.updateUser(messageObj[1], totalAttendance.length--)];
                                            case 4:
                                                deleteUserResponse = _a.sent();
                                                console.log("Rabbit: DELETE", deleteUserResponse.result.data);
                                                _a.label = 5;
                                            case 5: return [2 /*return*/];
                                        }
                                    });
                                });
                            })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    return RabbitService;
}());
exports.default = RabbitService;
