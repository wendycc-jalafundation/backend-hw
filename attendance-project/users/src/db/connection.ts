import { createConnection, Repository } from "typeorm";
import { User } from "../users/user.entity";

export class DbConnection {
    public static userRepository : Repository<User>;

  public static async initialize(): Promise<void> {
    await createConnection({
      type: "mysql",
      host: "localhost",
      port: 3306,
      username: "root",
      password: "root",
      database: "attendance",
      entities: ["dist/**/*.entity.js"],
      synchronize: true,
      logging: false,
    }).then(async (connection) => {
        this.userRepository = connection.getRepository(User)
      console.log(`Connected to DB`);
    });
  }

  
  
}
