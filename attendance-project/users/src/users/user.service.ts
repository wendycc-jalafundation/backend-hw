import { Repository } from "typeorm";
import { v4 as uuid } from "uuid";
import UserDTO from "./user.dto";
import { User } from "./user.entity";
import AttendanceService from "../attendance/service";
import UserAttendanceDTO from "./userAttendance.dto";

export default class UserService {
  private attendaceService = new AttendanceService();
  constructor(private repo: Repository<User>) {}
  async listUsers() {
    const getUsers = await this.repo.find();
    const simpleResult = getUsers.map((item) => {
      const userDTO: UserDTO = {
        id: item.id,
        nickname: item.nickName,
        attendance: item.attendance,
        name: item.firstName,
      };
      return userDTO;
    });
    return simpleResult;
  }

  async listUserWithAttendance(userId: string) {
    const getUser = (await this.repo.findOne({
      id: userId,
    })) as User;

    if (!getUser) return await {
      code: 404,
      message: `Could not find user with ID ${userId}`,
      data: {},
    };
    const attendance = await this.attendaceService.attendanceByUser(userId);
    return await {
      code: 200,
      message: "Listing user and attendance",
      data: {
        id: getUser.id,
        name: getUser.firstName,
        nickname: getUser.nickName,
        attendance: getUser.attendance,
        attendanceList: attendance.result,
      },
    };
  }

  async createUser(data: any) {
    const user = new User();
    user.id = uuid();
    user.firstName = data.firstName;
    user.nickName = data.nickName;
    return await this.repo.save(user);
  }
  async deleteUser(id: string) {
    const result = await this.repo.delete({ id: id });
    if (result.affected === 1) return true;
    return false;
  }

  async search(nickname?: string, name?: string) {
    let foundUsers = new Array<any>();
    if (nickname && name) {
      foundUsers = await this.repo.find({
        firstName: name,
        nickName: nickname,
      });
    }
    if (nickname && name == undefined)
      foundUsers = await this.repo.find({
        nickName: nickname,
      });

    if (name && nickname == undefined)
      foundUsers = await this.repo.find({
        firstName: name,
      });
    if (name == undefined && nickname == undefined)
      return await {
        code: 400,
        message: "Incorrect search criteria",
        data: {},
      };
    if (foundUsers.length === 0)
      return await {
        code: 404,
        message: "Could not find users with this criteria",
        data: {},
      };
    return await {
      code: 200,
      message: "Listing found users",
      data: foundUsers,
    };
  }

  async updateAttendance(userid: string, totalAttendance: number){

    const user : User = await this.repo.findOne({
      id : userid
    }) as User;
    
    user.attendance = totalAttendance;
    const updated = await this.repo.update(user.id as string, user)

    console.log(updated);
    if (updated.affected === 1) return true;
    return false;

  }
}
