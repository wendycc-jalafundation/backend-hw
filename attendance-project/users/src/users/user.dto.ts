export default class UserDTO{
    id?: string;
    nickname?:string;
    attendance?:number;
    name?:string;
}