export default class UserAttendanceDTO{
    id?: string;
    name?:string;
    nickname?:string;
    attendance?:number;
    attendanceList:any;
}