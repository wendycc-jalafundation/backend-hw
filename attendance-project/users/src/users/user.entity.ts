import {Entity, Column, PrimaryColumn} from "typeorm";

@Entity('User')
export class User{

    @PrimaryColumn()
    id?: string;

    @Column()
    firstName?: string;

    @Column()
    nickName?:string;

    @Column({default: 0})
    attendance?: number


}