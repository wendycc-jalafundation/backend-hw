import express, { Request, Response } from "express";
import { Repository } from "typeorm";
import { DbConnection } from "../db/connection";
import RabbitService from "../rabbit/rabbit.service";
import { User } from "./user.entity";
import UserService from "./user.service";

export default class UserController {
  path = "/users";
  router = express.Router();
  userService: UserService;
  constructor(private userRepo: Repository<User>, private rabbit: RabbitService) {
    this.userService = new UserService(userRepo);
    this.router.get(this.path, this.getUsers.bind(this));
    this.router.post(this.path, this.createUser.bind(this));
    this.router.delete(this.path+"/:id", this.deleteUser.bind(this));
    this.router.get(this.path+"/search", this.search.bind(this));
    this.router.get(this.path+'/:id', this.getUserAttendance.bind(this))
    this.router.put(this.path+'/:id', this.updateAttendance.bind(this))


  }

  async getUsers(_req: Request, res: Response) {
    try {
      const users = await this.userService.listUsers()
      res.status(200).json(users);
    } catch (e) {
      res.status(500);
    }
  }

  async createUser(req: Request, res: Response) {
    try {
      const result = await this.userService.createUser(req.body);
      res.status(200).json({
          code: 200,
          message: "User created",
          data: result
        });
    } catch (e) {
      res.status(500);
    }
  }
  async deleteUser(req:Request, res:Response){
    try {
        const result = await this.userService.deleteUser(req.params.id);
        if (result){
          this.rabbit.sendMessage(`delete_${req.params.id}`)
          res.status(200).json({
              code: 200,
              message: "User deleted",
              data: `User with ID ${req.params.id} deleted`
          });
        }
        else
        res.status(404).json({
            code: 404,
            error: `User with ID ${req.params.id} not found.`
        });
      } catch (e) {
        res.status(500);
      }
  }

  async search(req: Request, res:Response){
    try {
      const nickname = req.query.nickname as string;
      const name = req.query.name as string
      const result = await this.userService.search(nickname, name);
      if(result)
      {
        res.status(result.code).json(result)
      }
     
    } catch (e) {
      res.status(500);
    }
  }

  async getUserAttendance(req: Request, res: Response){
    try {
      
      const result = await this.userService.listUserWithAttendance(req.params.id)
      if(result)
      {
        res.status(200).json(result)
      }
     
    } catch (e) {
      res.status(500);
    }
  }

  async updateAttendance(req:Request, res: Response){
    try{
      const result = await this.userService.updateAttendance(req.params.id, req.body.attendance);
      if (result)
      res.status(200).json({
          code: 200,
          message: "User updated",
          data: `User with ID ${req.params.id} has attendance of ${req.body.attendance}`
      });
      else
      res.status(404).json({
          code: 404,
          error: `User with ID ${req.params.id} not found.`
      });
    } catch (e) {
      res.status(500);
    }
  }

}
