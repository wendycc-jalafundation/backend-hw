import { Options, Connection, Channel } from "amqplib";
import amqp = require("amqplib");

export default class RabbitService {
  connection!: Connection;
  channel!: Channel;
  async connect(): Promise<void> {
    const options: Options.Connect = {
      protocol: "amqp",
      hostname: "localhost",
      port: 5672,
      username: "admin",
      password: "admin",
    };
    try {
      this.connection = await amqp.connect(options);
      this.channel = await this.connection.createChannel();
      await this.channel.assertQueue("users", { durable: true });
      console.log("Rabbit is waiting for messages...");
    } catch (e) {
      console.log("Error connecting to Rabbit");
    }
  }
  sendMessage(message: any) {
    if (message) {
      this.channel.sendToQueue("users", Buffer.from(message));
      console.log(`Sending message: ${message}`);
      console.log("Rabbit is waiting for messages...");
    }
  }
}
