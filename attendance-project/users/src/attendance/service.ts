import * as rm from 'typed-rest-client/RestClient';

export default class AttendanceService{
    private connection = new rm.RestClient('attendance', 'http://localhost:3001');
    async attendanceByUser(userid: string){
        return await this.connection.get(`/attendance/user/${userid}`)
    }
}