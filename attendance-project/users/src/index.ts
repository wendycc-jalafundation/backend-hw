import App from "./app";
import UserController from "./users/user.controller";
import express, { Express, Request, Response } from 'express';
import dotenv from 'dotenv';
import { createConnection } from 'typeorm';
import { User } from './users/user.entity';
import RabbitService from "./rabbit/rabbit.service";

dotenv.config();

createConnection().then( async connection => {
  
  const userRepo = connection.getRepository(User);
  const rabbit = new RabbitService();
  rabbit.connect()

  const PORT = process.env.PORT || 3000;
 
  const app = new App([new UserController(userRepo, rabbit )], PORT);
  
  app.listen();
  
  
})





