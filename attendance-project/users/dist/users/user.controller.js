"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = __importDefault(require("express"));
var user_service_1 = __importDefault(require("./user.service"));
var UserController = /** @class */ (function () {
    function UserController(userRepo, rabbit) {
        this.userRepo = userRepo;
        this.rabbit = rabbit;
        this.path = "/users";
        this.router = express_1.default.Router();
        this.userService = new user_service_1.default(userRepo);
        this.router.get(this.path, this.getUsers.bind(this));
        this.router.post(this.path, this.createUser.bind(this));
        this.router.delete(this.path + "/:id", this.deleteUser.bind(this));
        this.router.get(this.path + "/search", this.search.bind(this));
        this.router.get(this.path + '/:id', this.getUserAttendance.bind(this));
        this.router.put(this.path + '/:id', this.updateAttendance.bind(this));
    }
    UserController.prototype.getUsers = function (_req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var users, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.userService.listUsers()];
                    case 1:
                        users = _a.sent();
                        res.status(200).json(users);
                        return [3 /*break*/, 3];
                    case 2:
                        e_1 = _a.sent();
                        res.status(500);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    UserController.prototype.createUser = function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var result, e_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.userService.createUser(req.body)];
                    case 1:
                        result = _a.sent();
                        res.status(200).json({
                            code: 200,
                            message: "User created",
                            data: result
                        });
                        return [3 /*break*/, 3];
                    case 2:
                        e_2 = _a.sent();
                        res.status(500);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    UserController.prototype.deleteUser = function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var result, e_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.userService.deleteUser(req.params.id)];
                    case 1:
                        result = _a.sent();
                        if (result) {
                            this.rabbit.sendMessage("delete_" + req.params.id);
                            res.status(200).json({
                                code: 200,
                                message: "User deleted",
                                data: "User with ID " + req.params.id + " deleted"
                            });
                        }
                        else
                            res.status(404).json({
                                code: 404,
                                error: "User with ID " + req.params.id + " not found."
                            });
                        return [3 /*break*/, 3];
                    case 2:
                        e_3 = _a.sent();
                        res.status(500);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    UserController.prototype.search = function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var nickname, name_1, result, e_4;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        nickname = req.query.nickname;
                        name_1 = req.query.name;
                        return [4 /*yield*/, this.userService.search(nickname, name_1)];
                    case 1:
                        result = _a.sent();
                        if (result) {
                            res.status(result.code).json(result);
                        }
                        return [3 /*break*/, 3];
                    case 2:
                        e_4 = _a.sent();
                        res.status(500);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    UserController.prototype.getUserAttendance = function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var result, e_5;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.userService.listUserWithAttendance(req.params.id)];
                    case 1:
                        result = _a.sent();
                        if (result) {
                            res.status(200).json(result);
                        }
                        return [3 /*break*/, 3];
                    case 2:
                        e_5 = _a.sent();
                        res.status(500);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    UserController.prototype.updateAttendance = function (req, res) {
        return __awaiter(this, void 0, void 0, function () {
            var result, e_6;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.userService.updateAttendance(req.params.id, req.body.attendance)];
                    case 1:
                        result = _a.sent();
                        if (result)
                            res.status(200).json({
                                code: 200,
                                message: "User updated",
                                data: "User with ID " + req.params.id + " has attendance of " + req.body.attendance
                            });
                        else
                            res.status(404).json({
                                code: 404,
                                error: "User with ID " + req.params.id + " not found."
                            });
                        return [3 /*break*/, 3];
                    case 2:
                        e_6 = _a.sent();
                        res.status(500);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    return UserController;
}());
exports.default = UserController;
