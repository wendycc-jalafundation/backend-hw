"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var uuid_1 = require("uuid");
var user_entity_1 = require("./user.entity");
var service_1 = __importDefault(require("../attendance/service"));
var UserService = /** @class */ (function () {
    function UserService(repo) {
        this.repo = repo;
        this.attendaceService = new service_1.default();
    }
    UserService.prototype.listUsers = function () {
        return __awaiter(this, void 0, void 0, function () {
            var getUsers, simpleResult;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.repo.find()];
                    case 1:
                        getUsers = _a.sent();
                        simpleResult = getUsers.map(function (item) {
                            var userDTO = {
                                id: item.id,
                                nickname: item.nickName,
                                attendance: item.attendance,
                                name: item.firstName,
                            };
                            return userDTO;
                        });
                        return [2 /*return*/, simpleResult];
                }
            });
        });
    };
    UserService.prototype.listUserWithAttendance = function (userId) {
        return __awaiter(this, void 0, void 0, function () {
            var getUser, attendance;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.repo.findOne({
                            id: userId,
                        })];
                    case 1:
                        getUser = (_a.sent());
                        if (!!getUser) return [3 /*break*/, 3];
                        return [4 /*yield*/, {
                                code: 404,
                                message: "Could not find user with ID " + userId,
                                data: {},
                            }];
                    case 2: return [2 /*return*/, _a.sent()];
                    case 3: return [4 /*yield*/, this.attendaceService.attendanceByUser(userId)];
                    case 4:
                        attendance = _a.sent();
                        return [4 /*yield*/, {
                                code: 200,
                                message: "Listing user and attendance",
                                data: {
                                    id: getUser.id,
                                    name: getUser.firstName,
                                    nickname: getUser.nickName,
                                    attendance: getUser.attendance,
                                    attendanceList: attendance.result,
                                },
                            }];
                    case 5: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    UserService.prototype.createUser = function (data) {
        return __awaiter(this, void 0, void 0, function () {
            var user;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        user = new user_entity_1.User();
                        user.id = uuid_1.v4();
                        user.firstName = data.firstName;
                        user.nickName = data.nickName;
                        return [4 /*yield*/, this.repo.save(user)];
                    case 1: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    UserService.prototype.deleteUser = function (id) {
        return __awaiter(this, void 0, void 0, function () {
            var result;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.repo.delete({ id: id })];
                    case 1:
                        result = _a.sent();
                        if (result.affected === 1)
                            return [2 /*return*/, true];
                        return [2 /*return*/, false];
                }
            });
        });
    };
    UserService.prototype.search = function (nickname, name) {
        return __awaiter(this, void 0, void 0, function () {
            var foundUsers;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        foundUsers = new Array();
                        if (!(nickname && name)) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.repo.find({
                                firstName: name,
                                nickName: nickname,
                            })];
                    case 1:
                        foundUsers = _a.sent();
                        _a.label = 2;
                    case 2:
                        if (!(nickname && name == undefined)) return [3 /*break*/, 4];
                        return [4 /*yield*/, this.repo.find({
                                nickName: nickname,
                            })];
                    case 3:
                        foundUsers = _a.sent();
                        _a.label = 4;
                    case 4:
                        if (!(name && nickname == undefined)) return [3 /*break*/, 6];
                        return [4 /*yield*/, this.repo.find({
                                firstName: name,
                            })];
                    case 5:
                        foundUsers = _a.sent();
                        _a.label = 6;
                    case 6:
                        if (!(name == undefined && nickname == undefined)) return [3 /*break*/, 8];
                        return [4 /*yield*/, {
                                code: 400,
                                message: "Incorrect search criteria",
                                data: {},
                            }];
                    case 7: return [2 /*return*/, _a.sent()];
                    case 8:
                        if (!(foundUsers.length === 0)) return [3 /*break*/, 10];
                        return [4 /*yield*/, {
                                code: 404,
                                message: "Could not find users with this criteria",
                                data: {},
                            }];
                    case 9: return [2 /*return*/, _a.sent()];
                    case 10: return [4 /*yield*/, {
                            code: 200,
                            message: "Listing found users",
                            data: foundUsers,
                        }];
                    case 11: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    UserService.prototype.updateAttendance = function (userid, totalAttendance) {
        return __awaiter(this, void 0, void 0, function () {
            var user, updated;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.repo.findOne({
                            id: userid
                        })];
                    case 1:
                        user = _a.sent();
                        user.attendance = totalAttendance;
                        return [4 /*yield*/, this.repo.update(user.id, user)];
                    case 2:
                        updated = _a.sent();
                        console.log(updated);
                        if (updated.affected === 1)
                            return [2 /*return*/, true];
                        return [2 /*return*/, false];
                }
            });
        });
    };
    return UserService;
}());
exports.default = UserService;
