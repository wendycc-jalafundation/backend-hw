import { INotificationChannel } from "./_notificationChannel";

export class SMSNotificationChannel implements INotificationChannel{
    private _notificationType : string = "SMS";

    get notificationType(){ return this._notificationType};

    notify(message: string){
        console.log(message);
    }
}