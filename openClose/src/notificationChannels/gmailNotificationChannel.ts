import { INotificationChannel } from "./_notificationChannel";

export class GmailNotificationChannel implements INotificationChannel{
    private _notificationType : string ="Gmail";

    get notificationType(){ return this._notificationType};

    notify(message: string){
        console.log(message);
    }
}