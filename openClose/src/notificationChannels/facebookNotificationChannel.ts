import { INotificationChannel } from "./_notificationChannel";

export class FacebookNotificationChannel implements INotificationChannel{
    private _notificationType : string = "Facebook";

    get notificationType(){ return this._notificationType};
    notify(message: string){
        console.log(message);
    }
}