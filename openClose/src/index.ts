import User from "./user";
import NotificationCenter from './notificationCenter';
import { SMSNotificationChannel } from "./notificationChannels/smsNotificationChannel";
import { FacebookNotificationChannel } from "./notificationChannels/facebookNotificationChannel";
import { GmailNotificationChannel } from "./notificationChannels/gmailNotificationChannel";


const smsNotification = new SMSNotificationChannel();
const facebookNotification = new FacebookNotificationChannel();
const gmailNotification = new GmailNotificationChannel();



const user = new User('Bob');
const notificationCenter = new NotificationCenter(user, 'testMessage');

notificationCenter.notify(smsNotification);
notificationCenter.notify(facebookNotification);
notificationCenter.notify(gmailNotification);

