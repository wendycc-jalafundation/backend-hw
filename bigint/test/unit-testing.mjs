import { assert, expect } from "chai"
import serialize from '../serialize.mjs'
import deserialize from '../deserialize.mjs'



describe("Testing", function () {
    const myJson = '{"testNumber":123,"testBigInt":"987n","nested":{"myProp":"5n","myProp2":10,"myArray":["5n"],"myObject":{"test":"5n"}},"myArray":[5,"50n"]}';
    const myObj = {
        testNumber: 123,
        testBigInt: 987n,
        nested: {
            myProp: 5n,
            myProp2: 10,
            myArray: [5n],
            myObject: {
                test: 5n
            }
        },
        myArray: [5, 50n]
    }
    it('Deserialize', function () {
   
        //assert.equal(myObj, deser(myJson), 'Deserialization error');
        
        expect(deserialize(myJson)).to.deep.equal(myObj);

    });
    it('Serialize', function () {
  
        expect(serialize(myObj)).to.deep.equal(myJson);

        //assert.equal(myJson, ser(myObj), 'Serialization error');
    });

    
    
})


