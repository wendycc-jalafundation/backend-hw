export default function serialize (myObj) {
    for (const property in myObj) {
        if (typeof myObj[property] === 'object' && myObj[property] != null)
            serialize(myObj[property])
        else if (typeof myObj[property] === 'bigint') {
            myObj[property] = myObj[property].toString() + 'n';
            
        }
    }
    return JSON.stringify(myObj)
}