import ser from './serialize.mjs'
import deser from './deserialize.mjs'
import {assert} from 'chai'
const myObj = {
     testNumber: 123,
     testBigInt: 987n,
     nested: {
       myProp: 5n,
       myProp2: 10,
       myArray: [5n],
       myObject: {
         test: 5n
       }
     },
     myArray: [5, 50n]
   }

const myJson = '{"testNumber":123,"testBigInt":"987n","nested":{"myProp":"5n","myProp2":10,"myArray":["5n"],"myObject":{"test":"5n"}},"myArray":[5,"50n"]}'

const serialize=ser(myObj);
const deserialize = deser(myJson);
console.log(serialize,'\n', deserialize)

// assert.equal(myJson, ser(myObj));
// assert.equal(myObj, deser(myJson));


  // Complex objects
  // Unit test
  // modules mjs
  // martes