# Chess API

## Milestones
1. Games are saved in the database using a MongoDB repository
2. These endpoints are working:
   1. getGame
   2. getGames
   3. newGame
   4. newTurnWhite (validation only for Pawn piece)
   5. newTurnBlack (validation only for the Pawn piece)
   
3. Validations:
   1. Validation of turn between black and white
   2. Validation for Pawn movements
4. Kills
   1. After validation a pawn can eat a piece
   
## C4 Model
### Level 1: System Context
![Leve 1](c4model/Chess-C1.png)

### Level 2: Container Diagram
![Level 2](c4model/Chess-C2.png)

### Level 3: Component diagram
![Level 3](c4model/Chess-C3.png)

### Level 4: Code