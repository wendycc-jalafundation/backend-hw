import { IPiece } from "./pieces/_piece.interface";
export default class Cell {
  private _color: string;
  private _row: number;
  private _column: number;
  private _piece: IPiece | null;
  
  constructor(colorSet: number, row: number, col: number) {
    this._color = colorSet == 1 ? "black" : "white";
    this._row = row;
    this._column = col;
    this._piece = null;
  }
  
  public get piece(): IPiece | null {
    return this._piece;
  }
  public set piece(value: IPiece | null) {
    this._piece = value;
  }
  public get color(): string {
    return this._color;
  }
  public set color(value: string) {
    this._color = value;
  }
  public get column(): number {
    return this._column;
  }
  public set column(value: number) {
    this._column = value;
  }
  public get row(): number {
    return this._row;
  }
  public set row(value: number) {
    this._row = value;
  }
 
}
