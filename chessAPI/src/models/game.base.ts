import Board from "./board";
import board from "./board";
import { ColorSet, GameStatus } from "./utils/enums";
import { IGame } from "./game.interface";
import Player from "./player";
import player from "./player";
import Turn from "./turn";
import turn from "./turn";

export abstract class BaseGame implements IGame {
  private _board: Board = new Board();
  private _playerBlack: Player = new Player();
  private _playerWhite: Player = new Player();
  private _gameStatus: GameStatus = GameStatus.ReadyToStart;
  private _gameID?: string;
  private _turns: Turn[] = new Array();
  private _currentTurn: ColorSet = ColorSet.Black;
  
  public get currentTurn(): ColorSet {
    return this._currentTurn;
  }
  public set currentTurn(value: ColorSet) {
    this._currentTurn = value;
  }

  constructor() {}

  public get gameID(): string {
    return this._gameID ?? (undefined as any);
  }

  public set gameID(value: string) {
    this._gameID = value;
  }

  public get gameStatus(): GameStatus {
    return this._gameStatus;
  }

  public set gameStatus(value: GameStatus) {
    this._gameStatus = value;
  }

  public get board() {
    return this._board;
  }
  public set board(value: Board) {
    this._board = value;
  }
  public get turns(): Turn[] {
    return this._turns;
  }
  public set turns(value: Turn[]) {
    this._turns = value;
  }
  public get playerWhite(): Player {
    return this._playerWhite;
  }
  public set playerWhite(value: Player) {
    this._playerWhite = value;
  }
  public get playerBlack(): Player {
    return this._playerBlack;
  }
  public set playerBlack(value: Player) {
    this._playerBlack = value;
  }
}
