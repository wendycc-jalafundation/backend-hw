import Board from "./board";
import { ColorSet, GameStatus } from "./utils/enums";
import Player from "./player";
import Turn from "./turn";

export interface IGame{
    board: Board;
    playerBlack:Player,
    playerWhite:Player,
    gameStatus: GameStatus,
    gameID:string,
    currentTurn: ColorSet,
    turns:Turn[]
}