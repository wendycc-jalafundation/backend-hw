export enum PieceType{
    King,
    Rook,
    Bishop,
    Queen,
    Knight,
    Pawn
};
export enum GameStatus{
    None,
    ReadyToStart,
    Playing,
    JaqueMate
}
export enum ColorSet{
    Black = 1,
    White = 0
}
export enum MoveType{
    Undefined,
    Normal,
    Kill,
    
}