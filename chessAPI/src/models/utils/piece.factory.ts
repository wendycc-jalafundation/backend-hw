import Cell from "../cell";
import {IPiece} from "../pieces/_piece.interface";

import { ColorSet, PieceType } from "./enums";
import { King } from "../pieces/king";
import { Queen } from "../pieces/queen";
import { Bishop } from "../pieces/bishop";
import { Knight } from "../pieces/knight";
import { Pawn } from "../pieces/pawn";
import { Rook } from "../pieces/rook";
import Board from "../board";

export default class Factory {
  public static allowedMovements(current: Cell, colorSet: ColorSet, board: Board): Cell[] {
    const movementPiece = current.piece?.pieceType;
    let pieceType : PieceType = 0;
    if (movementPiece){
          pieceType = movementPiece;
    }
    switch (pieceType) {
      case PieceType.King:
        return new King(colorSet).allowedMoves(current, board);
        break;
      case PieceType.Queen:
        return new Queen(colorSet).allowedMoves(current, board);
        break;
      case PieceType.Bishop:
        return new Bishop(colorSet).allowedMoves(current, board);
        break;
      case PieceType.Knight:
        return new Knight(colorSet).allowedMoves(current, board);
        break;
      case PieceType.Pawn:
        return new Pawn(colorSet).allowedMoves(current, board);
        break;
      case PieceType.Rook:
        return new Rook(colorSet).allowedMoves(current, board);
        break;
      default:
        return new King(colorSet).allowedMoves(current, board);
        break;
    }
  }

  public static createPiece(
    type: PieceType | undefined,
    colorSet: ColorSet | undefined
  ): IPiece {
    switch (type) {
      case PieceType.King:
        return new King(colorSet);
        break;
      case PieceType.Queen:
        return new Queen(colorSet);
        break;
      case PieceType.Bishop:
        return new Bishop(colorSet);
        break;
      case PieceType.Knight:
        return new Knight(colorSet);
        break;
      case PieceType.Pawn:
        return new Pawn(colorSet);
        break;
      case PieceType.Rook:
        return new Rook(colorSet);
        break;
      default:
        return new King(colorSet);
        break;
    }
  }
}
