import Cell from "./cell";
import { ColorSet, PieceType } from "./utils/enums";
import Factory from "./utils/piece.factory";

export default class Board {
  private _cells: Cell[] = new Array();
  private _size: number = 8;
  readonly BLACK_COLOR: number = 1;
  readonly WHITE_COLOR: number = 0;

  constructor() {}
  initializeCells() {
    let color = this.WHITE_COLOR;

    for (let i = 1; i <= this._size; i++) {
      for (let j = 1; j <= this._size; j++) {
        color = +!!!color;
        this._cells.push(new Cell(color, i, j));
      }
      color = +!!!color;
    }
  }

  initializeBlackPieces() {
    for (let i = 1; i <= this._size; i++) {
      this.getCell(7, i).piece = Factory.createPiece(
        PieceType.Pawn,
        ColorSet.Black
      );
    }
    this.getCell(8, 8).piece = Factory.createPiece(
      PieceType.Rook,
      ColorSet.Black
    );
    this.getCell(8, 1).piece = Factory.createPiece(
      PieceType.Rook,
      ColorSet.Black
    );
    this.getCell(8, 2).piece = Factory.createPiece(
      PieceType.Knight,
      ColorSet.Black
    );
    this.getCell(8, 7).piece = Factory.createPiece(
      PieceType.Knight,
      ColorSet.Black
    );
    this.getCell(8, 3).piece = Factory.createPiece(
      PieceType.Bishop,
      ColorSet.Black
    );
    this.getCell(8, 6).piece = Factory.createPiece(
      PieceType.Bishop,
      ColorSet.Black
    );
    this.getCell(8, 4).piece = Factory.createPiece(
      PieceType.King,
      ColorSet.Black
    );
    this.getCell(8, 5).piece = Factory.createPiece(
      PieceType.Queen,
      ColorSet.Black
    );
  }

  initializeWhitePieces() {
    for (let i = 1; i <= this._size; i++) {
      this.getCell(2, i).piece = Factory.createPiece(
        PieceType.Pawn,
        ColorSet.White
      );
    }
    this.getCell(1, 8).piece = Factory.createPiece(
      PieceType.Rook,
      ColorSet.White
    );
    this.getCell(1, 1).piece = Factory.createPiece(
      PieceType.Rook,
      ColorSet.White
    );
    this.getCell(1, 2).piece = Factory.createPiece(
      PieceType.Knight,
      ColorSet.White
    );
    this.getCell(1, 7).piece = Factory.createPiece(
      PieceType.Knight,
      ColorSet.White
    );
    this.getCell(1, 3).piece = Factory.createPiece(
      PieceType.Bishop,
      ColorSet.White
    );
    this.getCell(1, 6).piece = Factory.createPiece(
      PieceType.Bishop,
      ColorSet.White
    );
    this.getCell(1, 4).piece = Factory.createPiece(
      PieceType.King,
      ColorSet.White
    );
    this.getCell(1, 5).piece = Factory.createPiece(
      PieceType.Queen,
      ColorSet.White
    );
  }

  getCell(i: number, j: number): Cell {
    const dummyCell = new Cell(1, 1, 1);
    const cell: Cell | undefined = this._cells.find(
      (cell) => cell.row === i && cell.column === j
    );
    if (cell) return cell;
    return dummyCell;
  }

  public get cells(): Cell[] {
    return this._cells;
  }
  public set cells(value: Cell[]) {
    this._cells = value;
  }
  public get size(): number {
    return this._size;
  }
  public set size(value: number) {
    this._size = value;
  }
}
