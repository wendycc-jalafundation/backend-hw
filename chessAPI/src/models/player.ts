import { ColorSet} from "./utils/enums";

export default class Player {
  private _username: string = "";
  private _colorSet: ColorSet = ColorSet.Black;
  
  constructor() {}
  public get colorSet(): ColorSet {
    return this._colorSet;
  }
  public set colorSet(value: ColorSet) {
    this._colorSet = value;
  }
  public get username(): string {
    return this._username;
  }
  public set username(value: string) {
    this._username = value;
  }
  
}
