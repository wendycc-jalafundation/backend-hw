import Board from "./board";
import Player from "./player";
import Turn from "./turn";
import { GameStatus, MoveType, ColorSet } from "./utils/enums";
import Factory from "./utils/piece.factory";
import { IPiece } from "./pieces/_piece.interface";
import Cell from "./cell";
import { IGame } from "./game.interface";
import { BaseGame } from "./game.base";

export default class Game extends BaseGame {
  constructor() {
    super();
  }
  
  changeCurrentTurn( colorSet:ColorSet){
    super.currentTurn = colorSet;
  }
  initializeBoard() {
    super.board.initializeCells();
    super.board.initializeBlackPieces();
    super.board.initializeWhitePieces();
  }

  newTurn(turn: Turn) {
    super.turns.push(turn);
  }

  getCell(i: number, j: number): Cell {
    const dummyCell = new Cell(1, 1, 1);
    const cells: Cell[] = super.board.cells;
    const cell = cells.find((item: Cell) => {
      const row = item.row;
      const col = item.column;
      if (item.row === i && item.column === j) return item;
    });

    if (cell) return cell;
    return dummyCell;
  }
}
