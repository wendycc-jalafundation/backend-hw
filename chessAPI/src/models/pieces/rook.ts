import Board from "../board";
import Cell from "../cell";
import { ColorSet, PieceType } from "../utils/enums";
import { IPiece } from "./_piece.interface";

export class Rook implements IPiece {
  
    private _pieceType: PieceType = PieceType.Rook;
    constructor(private _colorSet: ColorSet | undefined){}
    get colorSet(): ColorSet {
      return this._colorSet ?? ColorSet.Black;
    }
    get pieceType(): PieceType {
      return this._pieceType;
    }
    allowedMoves(current: Cell, board: Board): Cell[] {
      const moves: Cell[] = [];
      return moves;
    }
  }