import Board from "../board";
import Cell from "../cell";
import {ColorSet, PieceType} from "../utils/enums";

export interface IPiece {
  pieceType: PieceType;
  colorSet: ColorSet;
  allowedMoves(current: Cell, board: Board): Cell[];
}









