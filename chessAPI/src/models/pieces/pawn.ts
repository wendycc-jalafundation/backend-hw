import Board from "../board";
import Cell from "../cell";
import { ColorSet, PieceType } from "../utils/enums";
import { IPiece } from "./_piece.interface";

export class Pawn implements IPiece {
  
    private _pieceType: PieceType = PieceType.Pawn;
    constructor(private _colorSet: ColorSet | undefined){}
    get colorSet(): ColorSet {
      return this._colorSet ?? ColorSet.Black;
    }
    get pieceType(): PieceType {
      return this._pieceType;
    }
    allowedMoves(current: Cell, board: Board): Cell[] {
        const row= current.row;
        const column = current.column;
        const colorSet : ColorSet | undefined= current.piece?.colorSet;
        const allowedMovements : Cell []= new Array();
        //Rule: The Pawn moves directly forward, never backward or to the side. 
        //Pawns capture a piece that is one square diagonally forward. 
        //Though Pawns normally cannot move diagonally, this is the only way they capture
        if ( colorSet === ColorSet.White)
        {

            const forwardCell = board.getCell(row+1, column);
            if (forwardCell.piece ==null)
                allowedMovements.push(forwardCell);
    
            let leftDiagonalCell;
            let rightDiagonalCell;
    
            if (column >=2)
                leftDiagonalCell = board.getCell(row+1, column-1);
            if (leftDiagonalCell?.piece != null)
                allowedMovements.push(leftDiagonalCell);
            
            if (column < board.size)
                rightDiagonalCell = board.getCell(row+1, column+1);
            if (rightDiagonalCell?.piece != null)
                allowedMovements.push(rightDiagonalCell);
        }
        else if (colorSet === ColorSet.Black)
        {
            const forwardCell = board.getCell(row-1, column);
            if (forwardCell.piece ==null)
                allowedMovements.push(forwardCell);
    
            let leftDiagonalCell;
            let rightDiagonalCell;
    
            if (column >=2)
                leftDiagonalCell = board.getCell(row-1, column-1);
            if (leftDiagonalCell?.piece != null)
                allowedMovements.push(leftDiagonalCell);
            
            if (column < board.size)
                rightDiagonalCell = board.getCell(row-1, column+1);
            if (rightDiagonalCell?.piece != null)
                allowedMovements.push(rightDiagonalCell);
        }

        return allowedMovements;
    }
    
  }