import e from "express";
import express, { Request, Response } from "express";
import Game from "../models/game";
import { BaseRepository } from "../repositories/baseRepository";
import { MongoDBRepository } from "../repositories/mongodb.repository";

import { collections } from "../services/database.service";
import GameService from "../services/game.services";

export default class GameController {
  public path = "/games";
  public router = express.Router();
  private gamesService: GameService;

  constructor() {
    const gamesRepository = new MongoDBRepository();
    this.gamesService = new GameService(gamesRepository);
    this.initializeRoutes();
  }

  initializeRoutes() {
    this.router.get(this.path, this.getGames.bind(this));
    this.router.post(this.path, this.newGame.bind(this));
    this.router.get(this.path + "/find", this.getGame.bind(this));
    this.router.post(this.path + "/turn/white", this.newTurnWhite.bind(this));
    this.router.post(this.path + "/turn/black", this.newTurnBlack.bind(this));
  }

  async getGame(_req: Request, res: Response) {
    try {
      debugger
      const game = await this.gamesService.findGame(_req.body.gameid);
      res.status(200).send(game);
    } catch (e) {
      res.status(500);
    }
  }
  async getGames(_req: Request, res: Response) {
    try {
      debugger
      const games = await this.gamesService.findAll();
      res.status(200).send(games);
    } catch (e) {
      res.status(500);
    }
  }

  async newGame(_req: Request, res: Response) {
    try {
      const result = await this.gamesService.newGame(
        _req.body.playerBlack,
        _req.body.playerWhite,
        _req.body.starts
      );
      result
        ? res.status(201).send(`Successfully created a new game`)
        : res.status(500).send("Failed to create a new game.");
    } catch (error) {
      console.error(error);
      res.status(400).send(error);
    }
  }
  async newTurnWhite(_req: Request, res: Response) {
    try {
      debugger;
      const validation = await this.gamesService.validateTurnWhite(
        _req.body.gameID
      )
      
      if (!validation)
      {
        res.status(500).send("It not your turn")
      }else{
        const moveValidation = await this.gamesService.validateMove(
          _req.body.gameID,
          _req.body.originPosition,
          _req.body.targetPosition
        )
        if (moveValidation){
          const result = await this.gamesService.executeNewTurnWhite(
            _req.body.gameID,
            _req.body.originPosition,
            _req.body.targetPosition
          );
          result
            ? res.status(201).send("Piece moved")
            : res.status(500).send("Error");
        }
        else{
          res.status(500).send("Invalid move");
        }

      }
      
    } catch (error) {
      console.error(error);
      res.status(400).send(error);
    }
  }

  async newTurnBlack(_req: Request, res: Response) {
    try {
      debugger;
      const validation = await this.gamesService.validateTurnBlack(
        _req.body.gameID
      )
      
      if (!validation)
      {
        res.status(500).send("It not your turn")
      }else{
        const moveValidation = await this.gamesService.validateMove(
          _req.body.gameID,
          _req.body.originPosition,
          _req.body.targetPosition
        )
        if (moveValidation){
          const result = await this.gamesService.executeNewTurnWhite(
            _req.body.gameID,
            _req.body.originPosition,
            _req.body.targetPosition
          );
          result
            ? res.status(201).send("Piece moved")
            : res.status(500).send("Error");
        }
        else{
          res.status(500).send("Invalid move");
        }

      }
      
    } catch (error) {
      console.error(error);
      res.status(400).send(error);
    }
  }
}
