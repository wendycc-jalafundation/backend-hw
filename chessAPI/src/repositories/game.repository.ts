import Board from "../models/board";
import Turn from "../models/turn";

export interface GameRepository<T>{
    updateBoard(id: any, board: Board): Promise<T>;
    updateTurns(id: any, turns:Turn[]): Promise<boolean>;
}