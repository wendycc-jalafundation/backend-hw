import BSON, { ObjectID } from "bson";
import e, { response } from "express";
import { ObjectId } from "mongodb";
import { resourceLimits } from "worker_threads";
import Board from "../models/board";
import Cell from "../models/cell";
import { ColorSet, PieceType } from "../models/utils/enums";
import Game from "../models/game";
import { IPiece } from "../models/pieces/_piece.interface";
import Factory from "../models/utils/piece.factory";
import Turn from "../models/turn";
import { collections } from "../services/database.service";
import { BaseRepository } from "./baseRepository";
import { GameRepository } from "./game.repository";
import Player from "../models/player";

export class MongoDBRepository
  implements BaseRepository<Game>, GameRepository<Game>
{
  async findOne(id: ObjectId): Promise<Game> {
    
    const result = await collections.games?.findOne({ _id: id });

    const deserializedGameObject: Game = this.deserializeGameObject(
      JSON.stringify(result)
    );
    return deserializedGameObject;
  }

  async create(game: Game): Promise<boolean> {
    const newGameID = new ObjectId();
    game.gameID = newGameID + "";
    const result = await collections.games?.insertOne({
      ...game,
      _id: newGameID,
    });
    return !!result?.insertedId;
  }

  async findAll(): Promise<Game[]> {
    debugger
    const result = await collections.games?.find({}).toArray();
    let games: Game[] = new Array();
    if (result?.length !==undefined && result.length > 0)
      for (let index = 0; index < result.length; index++) {
        const resultToJson = JSON.stringify(result[index]);
        const resultToGame: Game = this.deserializeGameObject(
          resultToJson
        );
        games.push(resultToGame);
      }
    return games;
  }

  async update(id: any, game: Game): Promise<Game> {
    const objectId = new ObjectID(id);
   
    const query = { _gameID: id };
    const replacement = {
      _board: game.board,
      _gameStatus: game.gameStatus,
      _turns : game.turns,
      _currentTurn : game.currentTurn
    };

    const result = await collections.games?.findOneAndUpdate(query, {
      $set: replacement,
    });

    return await this.findOne(objectId);
  }
  async updateBoard(id: string, board: Board): Promise<Game> {
    const objid = new ObjectID(id);
    const objboar = board;

    const query = { _gameID: id };
    const replacement = {
      _board: board,
    };

    const result = await collections.games?.findOneAndUpdate(query, {
      $set: replacement,
    });

    return await this.findOne(objid);
  }

  updateTurns(id: any, turns: Turn[]): Promise<boolean> {
    throw new Error("Method not implemented.");
  }
  deserializeGameObject(jsonString: string) {
    debugger;
    const game = JSON.parse(jsonString);
    const newGame = new Game();
    const board = new Board();

    let cells: Cell[] = new Array();
    cells = game._board._cells.map((cell: any) => {
      const colorSet = cell._color == "black" ? 1 : 0;
      const newCell = new Cell(colorSet, cell._row, cell._column);
      let newPiece: IPiece | null;
      if (cell._piece != null) {
        newPiece = Factory.createPiece(
          cell._piece._pieceType,
          cell._piece._colorSet
        );
      } else {
        newPiece = null;
      }
      newCell.piece = newPiece;
      return newCell;
    });
    board.cells = cells;
    newGame.board = board;
    newGame.gameStatus = game._gameStatus;
    newGame.currentTurn = game._currentTurn;
    newGame.gameID = game._gameID;

    const playerBlack = new Player();
    playerBlack.username = game._playerBlack._username;
    playerBlack.colorSet = ColorSet.Black;
    const playerWhite = new Player();
    playerWhite.username = game._playerWhite._username;
    playerBlack.colorSet = ColorSet.White;

    newGame.playerBlack = playerBlack;
    newGame.playerWhite = playerWhite;

    let turns: Turn[] = new Array();
    turns = game._turns.map((turn: any) => {
      let cells: Cell[] = new Array();
      cells = game._board._cells.map((cell: any) => {
        const colorSet = cell._color == "black" ? 1 : 0;
        const newCell = new Cell(colorSet, cell._row, cell._column);
        let newPiece: IPiece | null;
        if (cell._piece != null) {
          newPiece = Factory.createPiece(
            cell._piece._pieceType,
            cell._piece._colorSet
          );
        } else {
          newPiece = null;
        }
        newCell.piece = newPiece;
        return newCell;
      });
      const board = new Board();
      board.cells = cells;
      let piece: IPiece | null;
      if (turn._killedPiece != null) {
        piece = Factory.createPiece(
          turn._killedPiece._pieceType,
          turn._killedPiece._colorSet
        );
      } else {
        piece = null;
      }

      const newTurn = new Turn(turn._player, board, piece);

      return newTurn;
    });
    board.cells = cells;
    board.size = game._board._size;
    newGame.board = board;
    newGame.gameStatus = game._gameStatus;
    newGame.gameID = game._gameID;

    newGame.turns = turns;
    return newGame;
  }
}
