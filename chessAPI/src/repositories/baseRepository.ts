import Board from "../models/board";
import Game from "../models/game";
import Turn from "../models/turn";

export interface BaseRepository<T> {
    findAll():Promise<T[]>;
    findOne(id:any): Promise<T>;
    create(item:T):Promise<boolean>;
    update(id:any, item:T): Promise<T>;
}