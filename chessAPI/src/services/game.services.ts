import { ObjectID } from "bson";
import { response } from "express";
import { start } from "repl";
import Board from "../models/board";
import Cell from "../models/cell";
import { ColorSet, GameStatus, MoveType } from "../models/utils/enums";
import Game from "../models/game";
import { IPiece } from "../models/pieces/_piece.interface";
import Factory from "../models/utils/piece.factory";
import Player from "../models/player";
import Turn from "../models/turn";
import { BaseRepository } from "../repositories/baseRepository";
import { GameRepository } from "../repositories/game.repository";
import { Chess } from "./chess.service";

export default class GameService {
  constructor(
    private gameRepository: BaseRepository<Game> & GameRepository<Game>
  ) {}

  async findAll(): Promise<Game[]> {
    return this.gameRepository.findAll();
  }

  async newGame(
    playerBlack: string,
    playerWhite: string,
    starts: number
  ): Promise<boolean> {
    const newGame = new Game();
    newGame.playerBlack.username = playerBlack;
    newGame.playerWhite.username = playerWhite;
    newGame.currentTurn = starts;
    newGame.initializeBoard();
    return this.gameRepository.create(newGame);
  }

  async findGame(id: string): Promise<Game> {
    const objecctId = new ObjectID(id);
    return this.gameRepository.findOne(objecctId);
  }

  async executeNewTurnWhite(
    gameID: string,
    originCoordinates: string,
    targetCoordinates: string
  ): Promise<Game> {
    debugger;
    const game: Game = await this.findGame(gameID);
    const chess = new Chess(game);

    const pieceKilled: IPiece | null = chess.executeTurn(
      ColorSet.White,
      originCoordinates,
      targetCoordinates
    );

    const newTurn = new Turn(ColorSet.White, game.board, pieceKilled);
    game.turns.push(newTurn);
    game.gameStatus = GameStatus.Playing;
    game.currentTurn = ColorSet.Black;
    const updatedGame = await this.gameRepository.update(gameID, game);
    return updatedGame;
  }

  async executeNewTurnBlack(
    gameID: string,
    originCoordinates: string,
    targetCoordinates: string
  ): Promise<Game> {
    debugger;
    const game: Game = await this.findGame(gameID);
    const chess = new Chess(game);
    const pieceKilled: IPiece | null = chess.executeTurn(
      ColorSet.Black,
      originCoordinates,
      targetCoordinates
    );

    const newTurn = new Turn(ColorSet.Black, game.board, pieceKilled);
    game.turns.push(newTurn);
    game.gameStatus = GameStatus.Playing;
    game.currentTurn = ColorSet.White;
    const updatedGame = await this.gameRepository.update(gameID, game);

    return updatedGame;
  }

  async validateTurnWhite(gameID: string): Promise<boolean> {
    debugger;
    const game: Game = await this.findGame(gameID);
    if (game.currentTurn != ColorSet.White) return false;
    return true;
  }

  async validateTurnBlack(gameID: string): Promise<boolean> {
    debugger;
    const game: Game = await this.findGame(gameID);
    if (game.currentTurn != ColorSet.Black) return false;
    return true;
  }

  async validateMove(
    gameID: string,
    originCoordinates: string,
    targetCoordinates: string
  ): Promise<boolean> {
    const game: Game = await this.findGame(gameID);
    const chess = new Chess(game);
    const isValid = chess.moveValidationforGameService(0, originCoordinates, targetCoordinates);
    return isValid;
  }
}
