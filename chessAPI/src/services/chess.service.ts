import { debug } from "console";
import { kill } from "process";
import Board from "../models/board";
import Cell from "../models/cell";
import { ColorSet, GameStatus, MoveType } from "../models/utils/enums";
import Game from "../models/game";
import { IPiece } from "../models/pieces/_piece.interface";
import Factory from "../models/utils/piece.factory";

export class Chess {
  private game: Game;

  constructor(game: Game) {
    this.game = game;
  }
  executeTurn(
    colorSet: ColorSet,
    originCoordinates: string,
    targetCoordinates: string
  ): IPiece | null {
    debugger
    console.log("inside chess", this.game.board);
    const movementCells = this.getMovementCells(
      originCoordinates,
      targetCoordinates
    );

    const origin: Cell = movementCells.origin;
    const target: Cell = movementCells.target;

    const isValid = this.moveValidation(colorSet, origin, target);

    let moveType: MoveType = MoveType.Undefined;
    if (isValid) {
      moveType = this.clasifyMove(target);
    }

    let killedPiece: IPiece | null = null;
    if (moveType === MoveType.Normal) {
      this.executeNormalMove(origin, target);
    } else if (moveType === MoveType.Kill) {
      killedPiece = this.executeKillMove(origin, target);
    }
    return killedPiece;
  }

  changeBoardState(newBoardState: Board, gameStatus: GameStatus, game: Game) {
    game.board = newBoardState;
    game.gameStatus = gameStatus;
  }

  clasifyMove(target: Cell): MoveType {
    if (target.piece === null) {
      return MoveType.Normal;
    }
    return MoveType.Kill;
  }
  executeNormalMove(origin: Cell, target: Cell) {
    target.piece = Factory.createPiece(
      origin.piece?.pieceType,
      origin.piece?.colorSet
    );
    origin.piece = null;
  }

  executeKillMove(origin: Cell, target: Cell): IPiece {
    const killedPiece: IPiece = Factory.createPiece(
      target.piece?.pieceType,
      target.piece?.colorSet
    );
    target.piece = Factory.createPiece(
      origin.piece?.pieceType,
      origin.piece?.colorSet
    );
    origin.piece = null;
    return killedPiece;
  }

  generateCoordinates(moveCoordinates: string) {
    const originCoordinates = moveCoordinates.split("-");
    const row = +originCoordinates[0];
    const column = +originCoordinates[1];

    return {
      row: row,
      column: column,
    };
  }

  getMovementCells(originCoordinates: string, targetCoordinates: string) {
    console.log("inside chess/getmovements", this.game.board);

    const getOriginCell = this.generateCoordinates(originCoordinates);
    const getTargetCell = this.generateCoordinates(targetCoordinates);

    const originCell = this.game.getCell(
      getOriginCell.row,
      getOriginCell.column
    );
    const targetCell = this.game.getCell(
      getTargetCell.row,
      getTargetCell.column
    );

    return {
      origin: originCell,
      target: targetCell,
    };
  }

  moveValidation(colorSet: ColorSet, origin: Cell, target: Cell): boolean {
    const originCell = this.game.getCell(origin.row, origin.column);
    const targetCell = this.game.getCell(target.row, target.column);
    const allowedMoves = Factory.allowedMovements(
      originCell,
      colorSet,
      this.game.board
    );

    const validateMove = allowedMoves.find(
      (cell) => cell.row === targetCell.row && cell.column === targetCell.column
    );
    // TODO: validate if player is white or black
    if (validateMove) return true;
    return false;
  }

  moveValidationforGameService(
    colorSet: ColorSet,
    originCoordinates: string,
    targetCoordinates: string
  ): boolean {
    let isValid = true;

    console.log("inside chess", this.game.board);
    const movementCells = this.getMovementCells(
      originCoordinates,
      targetCoordinates
    );

    const origin: Cell = movementCells.origin;
    const target: Cell = movementCells.target;

    isValid = this.moveValidation(colorSet, origin, target);

    return isValid;
  }
}
