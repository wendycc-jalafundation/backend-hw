import * as mongoDB from "mongodb";
import * as dotenv from "dotenv";

export const collections: { games?: mongoDB.Collection } = {};


export async function connectToDatabase() {
  dotenv.config();

  const client: mongoDB.MongoClient = new mongoDB.MongoClient(
    "mongodb://localhost:27017/"
  );

  await client.connect();

  const db: mongoDB.Db = client.db("chess");

  const gamesCollection: mongoDB.Collection = db.collection("games");

  collections.games = gamesCollection;

  console.log(
    `Successfully connected to database: ${db.databaseName} and collection: ${gamesCollection.collectionName}`
  );
}
