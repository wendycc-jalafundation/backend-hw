import App from "./app";
import GameController from "./controllers/game.controller";



const PORT = process.env.PORT || 8080;

const app = new App(
  [
    new GameController()
  ],
  PORT,
);
 
app.listen();




