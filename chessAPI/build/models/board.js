"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const cell_1 = __importDefault(require("./cell"));
const enums_1 = require("./utils/enums");
const piece_factory_1 = __importDefault(require("./utils/piece.factory"));
class Board {
    constructor() {
        this._cells = new Array();
        this._size = 8;
        this.BLACK_COLOR = 1;
        this.WHITE_COLOR = 0;
    }
    initializeCells() {
        let color = this.WHITE_COLOR;
        for (let i = 1; i <= this._size; i++) {
            for (let j = 1; j <= this._size; j++) {
                color = +!!!color;
                this._cells.push(new cell_1.default(color, i, j));
            }
            color = +!!!color;
        }
    }
    initializeBlackPieces() {
        for (let i = 1; i <= this._size; i++) {
            this.getCell(7, i).piece = piece_factory_1.default.createPiece(enums_1.PieceType.Pawn, enums_1.ColorSet.Black);
        }
        this.getCell(8, 8).piece = piece_factory_1.default.createPiece(enums_1.PieceType.Rook, enums_1.ColorSet.Black);
        this.getCell(8, 1).piece = piece_factory_1.default.createPiece(enums_1.PieceType.Rook, enums_1.ColorSet.Black);
        this.getCell(8, 2).piece = piece_factory_1.default.createPiece(enums_1.PieceType.Knight, enums_1.ColorSet.Black);
        this.getCell(8, 7).piece = piece_factory_1.default.createPiece(enums_1.PieceType.Knight, enums_1.ColorSet.Black);
        this.getCell(8, 3).piece = piece_factory_1.default.createPiece(enums_1.PieceType.Bishop, enums_1.ColorSet.Black);
        this.getCell(8, 6).piece = piece_factory_1.default.createPiece(enums_1.PieceType.Bishop, enums_1.ColorSet.Black);
        this.getCell(8, 4).piece = piece_factory_1.default.createPiece(enums_1.PieceType.King, enums_1.ColorSet.Black);
        this.getCell(8, 5).piece = piece_factory_1.default.createPiece(enums_1.PieceType.Queen, enums_1.ColorSet.Black);
    }
    initializeWhitePieces() {
        for (let i = 1; i <= this._size; i++) {
            this.getCell(2, i).piece = piece_factory_1.default.createPiece(enums_1.PieceType.Pawn, enums_1.ColorSet.White);
        }
        this.getCell(1, 8).piece = piece_factory_1.default.createPiece(enums_1.PieceType.Rook, enums_1.ColorSet.White);
        this.getCell(1, 1).piece = piece_factory_1.default.createPiece(enums_1.PieceType.Rook, enums_1.ColorSet.White);
        this.getCell(1, 2).piece = piece_factory_1.default.createPiece(enums_1.PieceType.Knight, enums_1.ColorSet.White);
        this.getCell(1, 7).piece = piece_factory_1.default.createPiece(enums_1.PieceType.Knight, enums_1.ColorSet.White);
        this.getCell(1, 3).piece = piece_factory_1.default.createPiece(enums_1.PieceType.Bishop, enums_1.ColorSet.White);
        this.getCell(1, 6).piece = piece_factory_1.default.createPiece(enums_1.PieceType.Bishop, enums_1.ColorSet.White);
        this.getCell(1, 4).piece = piece_factory_1.default.createPiece(enums_1.PieceType.King, enums_1.ColorSet.White);
        this.getCell(1, 5).piece = piece_factory_1.default.createPiece(enums_1.PieceType.Queen, enums_1.ColorSet.White);
    }
    getCell(i, j) {
        const dummyCell = new cell_1.default(1, 1, 1);
        const cell = this._cells.find((cell) => cell.row === i && cell.column === j);
        if (cell)
            return cell;
        return dummyCell;
    }
    get cells() {
        return this._cells;
    }
    set cells(value) {
        this._cells = value;
    }
    get size() {
        return this._size;
    }
    set size(value) {
        this._size = value;
    }
}
exports.default = Board;
//# sourceMappingURL=board.js.map