"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const cell_1 = __importDefault(require("./cell"));
const game_base_1 = require("./game.base");
class Game extends game_base_1.BaseGame {
    constructor() {
        super();
    }
    changeCurrentTurn(colorSet) {
        super.currentTurn = colorSet;
    }
    initializeBoard() {
        super.board.initializeCells();
        super.board.initializeBlackPieces();
        super.board.initializeWhitePieces();
    }
    newTurn(turn) {
        super.turns.push(turn);
    }
    getCell(i, j) {
        const dummyCell = new cell_1.default(1, 1, 1);
        const cells = super.board.cells;
        const cell = cells.find((item) => {
            const row = item.row;
            const col = item.column;
            if (item.row === i && item.column === j)
                return item;
        });
        if (cell)
            return cell;
        return dummyCell;
    }
}
exports.default = Game;
//# sourceMappingURL=game.js.map