"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Pawn = void 0;
const enums_1 = require("../utils/enums");
class Pawn {
    constructor(_colorSet) {
        this._colorSet = _colorSet;
        this._pieceType = enums_1.PieceType.Pawn;
    }
    get colorSet() {
        var _a;
        return (_a = this._colorSet) !== null && _a !== void 0 ? _a : enums_1.ColorSet.Black;
    }
    get pieceType() {
        return this._pieceType;
    }
    allowedMoves(current, board) {
        var _a;
        const row = current.row;
        const column = current.column;
        const colorSet = (_a = current.piece) === null || _a === void 0 ? void 0 : _a.colorSet;
        const allowedMovements = new Array();
        //Rule: The Pawn moves directly forward, never backward or to the side. 
        //Pawns capture a piece that is one square diagonally forward. 
        //Though Pawns normally cannot move diagonally, this is the only way they capture
        if (colorSet === enums_1.ColorSet.White) {
            const forwardCell = board.getCell(row + 1, column);
            if (forwardCell.piece == null)
                allowedMovements.push(forwardCell);
            let leftDiagonalCell;
            let rightDiagonalCell;
            if (column >= 2)
                leftDiagonalCell = board.getCell(row + 1, column - 1);
            if ((leftDiagonalCell === null || leftDiagonalCell === void 0 ? void 0 : leftDiagonalCell.piece) != null)
                allowedMovements.push(leftDiagonalCell);
            if (column < board.size)
                rightDiagonalCell = board.getCell(row + 1, column + 1);
            if ((rightDiagonalCell === null || rightDiagonalCell === void 0 ? void 0 : rightDiagonalCell.piece) != null)
                allowedMovements.push(rightDiagonalCell);
        }
        else if (colorSet === enums_1.ColorSet.Black) {
            const forwardCell = board.getCell(row - 1, column);
            if (forwardCell.piece == null)
                allowedMovements.push(forwardCell);
            let leftDiagonalCell;
            let rightDiagonalCell;
            if (column >= 2)
                leftDiagonalCell = board.getCell(row - 1, column - 1);
            if ((leftDiagonalCell === null || leftDiagonalCell === void 0 ? void 0 : leftDiagonalCell.piece) != null)
                allowedMovements.push(leftDiagonalCell);
            if (column < board.size)
                rightDiagonalCell = board.getCell(row - 1, column + 1);
            if ((rightDiagonalCell === null || rightDiagonalCell === void 0 ? void 0 : rightDiagonalCell.piece) != null)
                allowedMovements.push(rightDiagonalCell);
        }
        return allowedMovements;
    }
}
exports.Pawn = Pawn;
//# sourceMappingURL=pawn.js.map