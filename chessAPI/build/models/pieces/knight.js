"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Knight = void 0;
const enums_1 = require("../utils/enums");
class Knight {
    constructor(_colorSet) {
        this._colorSet = _colorSet;
        this._pieceType = enums_1.PieceType.Knight;
    }
    get colorSet() {
        var _a;
        return (_a = this._colorSet) !== null && _a !== void 0 ? _a : enums_1.ColorSet.Black;
    }
    get pieceType() {
        return this._pieceType;
    }
    allowedMoves(current, board) {
        const moves = [];
        return moves;
    }
}
exports.Knight = Knight;
//# sourceMappingURL=knight.js.map