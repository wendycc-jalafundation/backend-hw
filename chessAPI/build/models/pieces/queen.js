"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Queen = void 0;
const enums_1 = require("../utils/enums");
class Queen {
    constructor(_colorSet) {
        this._colorSet = _colorSet;
        this._pieceType = enums_1.PieceType.Queen;
    }
    get colorSet() {
        var _a;
        return (_a = this._colorSet) !== null && _a !== void 0 ? _a : enums_1.ColorSet.Black;
    }
    get pieceType() {
        return this._pieceType;
    }
    allowedMoves(current, board) {
        const moves = [];
        return moves;
    }
}
exports.Queen = Queen;
//# sourceMappingURL=queen.js.map