"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const piece_1 = require("./piece");
const enums_1 = require("./enums");
class Factory {
    static allowedMovements(current, type) {
        if (type === enums_1.PieceType.King) {
            return new piece_1.King().allowedMoves(current);
        }
        else if (type === enums_1.PieceType.Queen) {
            return new piece_1.Queen().allowedMoves(current);
        }
        return new piece_1.King().allowedMoves(current);
    }
    static createPiece(type) {
        const piecetemp = new piece_1.King();
        if (type === enums_1.PieceType.King) {
            return new piece_1.King();
        }
        else if (type === enums_1.PieceType.Queen) {
            return new piece_1.Queen();
        }
        return piecetemp;
    }
}
exports.default = Factory;
