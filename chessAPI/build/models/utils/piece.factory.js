"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const enums_1 = require("./enums");
const king_1 = require("../pieces/king");
const queen_1 = require("../pieces/queen");
const bishop_1 = require("../pieces/bishop");
const knight_1 = require("../pieces/knight");
const pawn_1 = require("../pieces/pawn");
const rook_1 = require("../pieces/rook");
class Factory {
    static allowedMovements(current, colorSet, board) {
        var _a;
        const movementPiece = (_a = current.piece) === null || _a === void 0 ? void 0 : _a.pieceType;
        let pieceType = 0;
        if (movementPiece) {
            pieceType = movementPiece;
        }
        switch (pieceType) {
            case enums_1.PieceType.King:
                return new king_1.King(colorSet).allowedMoves(current, board);
                break;
            case enums_1.PieceType.Queen:
                return new queen_1.Queen(colorSet).allowedMoves(current, board);
                break;
            case enums_1.PieceType.Bishop:
                return new bishop_1.Bishop(colorSet).allowedMoves(current, board);
                break;
            case enums_1.PieceType.Knight:
                return new knight_1.Knight(colorSet).allowedMoves(current, board);
                break;
            case enums_1.PieceType.Pawn:
                return new pawn_1.Pawn(colorSet).allowedMoves(current, board);
                break;
            case enums_1.PieceType.Rook:
                return new rook_1.Rook(colorSet).allowedMoves(current, board);
                break;
            default:
                return new king_1.King(colorSet).allowedMoves(current, board);
                break;
        }
    }
    static createPiece(type, colorSet) {
        switch (type) {
            case enums_1.PieceType.King:
                return new king_1.King(colorSet);
                break;
            case enums_1.PieceType.Queen:
                return new queen_1.Queen(colorSet);
                break;
            case enums_1.PieceType.Bishop:
                return new bishop_1.Bishop(colorSet);
                break;
            case enums_1.PieceType.Knight:
                return new knight_1.Knight(colorSet);
                break;
            case enums_1.PieceType.Pawn:
                return new pawn_1.Pawn(colorSet);
                break;
            case enums_1.PieceType.Rook:
                return new rook_1.Rook(colorSet);
                break;
            default:
                return new king_1.King(colorSet);
                break;
        }
    }
}
exports.default = Factory;
//# sourceMappingURL=piece.factory.js.map