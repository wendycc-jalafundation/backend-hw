"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MoveType = exports.ColorSet = exports.GameStatus = exports.PieceType = void 0;
var PieceType;
(function (PieceType) {
    PieceType[PieceType["King"] = 0] = "King";
    PieceType[PieceType["Rook"] = 1] = "Rook";
    PieceType[PieceType["Bishop"] = 2] = "Bishop";
    PieceType[PieceType["Queen"] = 3] = "Queen";
    PieceType[PieceType["Knight"] = 4] = "Knight";
    PieceType[PieceType["Pawn"] = 5] = "Pawn";
})(PieceType = exports.PieceType || (exports.PieceType = {}));
;
var GameStatus;
(function (GameStatus) {
    GameStatus[GameStatus["None"] = 0] = "None";
    GameStatus[GameStatus["ReadyToStart"] = 1] = "ReadyToStart";
    GameStatus[GameStatus["Playing"] = 2] = "Playing";
    GameStatus[GameStatus["JaqueMate"] = 3] = "JaqueMate";
})(GameStatus = exports.GameStatus || (exports.GameStatus = {}));
var ColorSet;
(function (ColorSet) {
    ColorSet[ColorSet["Black"] = 1] = "Black";
    ColorSet[ColorSet["White"] = 0] = "White";
})(ColorSet = exports.ColorSet || (exports.ColorSet = {}));
var MoveType;
(function (MoveType) {
    MoveType[MoveType["Undefined"] = 0] = "Undefined";
    MoveType[MoveType["Normal"] = 1] = "Normal";
    MoveType[MoveType["Kill"] = 2] = "Kill";
})(MoveType = exports.MoveType || (exports.MoveType = {}));
//# sourceMappingURL=enums.js.map