"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const enums_1 = require("./utils/enums");
class Player {
    constructor() {
        this._username = "";
        this._colorSet = enums_1.ColorSet.Black;
    }
    get colorSet() {
        return this._colorSet;
    }
    set colorSet(value) {
        this._colorSet = value;
    }
    get username() {
        return this._username;
    }
    set username(value) {
        this._username = value;
    }
}
exports.default = Player;
//# sourceMappingURL=player.js.map