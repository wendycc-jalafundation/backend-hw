"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Turn = exports.Status = exports.Player = exports.Knight = exports.Pawn = exports.Rook = exports.Bishop = exports.Queen = exports.King = exports.Factory = exports.Game = exports.GameStatus = exports.PieceType = exports.Board = exports.Cell = void 0;
class Cell {
    constructor(color, row, col) {
        this._color = color == 1 ? "black" : "white";
        this._row = row;
        this._column = col;
        this._piece = null;
    }
    get row() {
        return this._row;
    }
    get col() {
        return this._column;
    }
    set piece(piece) {
        this._piece = piece;
    }
}
exports.Cell = Cell;
class Board {
    constructor() {
        this.cells = new Array();
        this.size = 8;
        this.BLACK_COLOR = 1;
        this.WHITE_COLOR = 0;
    }
    initializeCells() {
        let color = this.WHITE_COLOR;
        for (let i = 1; i <= this.size; i++) {
            for (let j = 1; j <= this.size; j++) {
                color = +!!!color;
                this.cells.push(new Cell(color, i, j));
            }
            color = +!!!color;
        }
    }
    initializeBlackPieces() {
        for (let i = 1; i <= this.size; i++) {
            this.getCell(7, i).piece = Factory.createPiece(PieceType.Pawn);
        }
        this.getCell(8, 8).piece = Factory.createPiece(PieceType.Rook);
        this.getCell(8, 1).piece = Factory.createPiece(PieceType.Rook);
        this.getCell(8, 2).piece = Factory.createPiece(PieceType.Knight);
        this.getCell(8, 7).piece = Factory.createPiece(PieceType.Knight);
        this.getCell(8, 3).piece = Factory.createPiece(PieceType.Bishop);
        this.getCell(8, 6).piece = Factory.createPiece(PieceType.Bishop);
        this.getCell(8, 4).piece = Factory.createPiece(PieceType.King);
        this.getCell(8, 5).piece = Factory.createPiece(PieceType.Queen);
    }
    initializeWhitePieces() {
        for (let i = 1; i <= this.size; i++) {
            this.getCell(2, i).piece = Factory.createPiece(PieceType.Pawn);
        }
        this.getCell(1, 8).piece = Factory.createPiece(PieceType.Rook);
        this.getCell(1, 1).piece = Factory.createPiece(PieceType.Rook);
        this.getCell(1, 2).piece = Factory.createPiece(PieceType.Knight);
        this.getCell(1, 7).piece = Factory.createPiece(PieceType.Knight);
        this.getCell(1, 3).piece = Factory.createPiece(PieceType.Bishop);
        this.getCell(1, 6).piece = Factory.createPiece(PieceType.Bishop);
        this.getCell(1, 4).piece = Factory.createPiece(PieceType.King);
        this.getCell(1, 5).piece = Factory.createPiece(PieceType.Queen);
    }
    getCell(i, j) {
        var _a;
        return (_a = this.cells.find((cell) => cell.row === i && cell.col === j)) !== null && _a !== void 0 ? _a : new Cell(1, 1, 1);
    }
}
exports.Board = Board;
var PieceType;
(function (PieceType) {
    PieceType[PieceType["King"] = 0] = "King";
    PieceType[PieceType["Rook"] = 1] = "Rook";
    PieceType[PieceType["Bishop"] = 2] = "Bishop";
    PieceType[PieceType["Queen"] = 3] = "Queen";
    PieceType[PieceType["Knight"] = 4] = "Knight";
    PieceType[PieceType["Pawn"] = 5] = "Pawn";
})(PieceType = exports.PieceType || (exports.PieceType = {}));
;
var GameStatus;
(function (GameStatus) {
    GameStatus[GameStatus["None"] = 0] = "None";
    GameStatus[GameStatus["ReadyToStart"] = 1] = "ReadyToStart";
    GameStatus[GameStatus["Playing"] = 2] = "Playing";
    GameStatus[GameStatus["JaqueMate"] = 3] = "JaqueMate";
})(GameStatus = exports.GameStatus || (exports.GameStatus = {}));
//export {PieceType, GameStatus};
class Game {
    //private turns:Turn[];
    constructor(playerOneUsername, playerTwoUsername, _id) {
        this._id = _id;
        this._board = new Board();
        this._playerOne = new Player(playerOneUsername);
        this._playerTwo = new Player(playerTwoUsername);
        this._gameStatus = GameStatus.ReadyToStart;
    }
    initializeBoard() {
        this._board.initializeCells();
        this._board.initializeBlackPieces();
        this._board.initializeWhitePieces();
    }
    //#region Getters and Setters
    get id() {
        var _a;
        return (_a = this._id) !== null && _a !== void 0 ? _a : undefined;
    }
    set id(value) {
        this._id = value;
    }
    get gameStatus() {
        return this._gameStatus;
    }
    set gameStatus(value) {
        this._gameStatus = value;
    }
    get playerTwo() {
        return this._playerTwo;
    }
    set playerTwo(value) {
        this._playerTwo = value;
    }
    get playerOne() {
        return this._playerOne;
    }
    set playerOne(value) {
        this._playerOne = value;
    }
    get board() {
        return this._board;
    }
}
exports.Game = Game;
class Factory {
    static allowedMovements(current, type) {
        if (type === PieceType.King) {
            return new King().allowedMoves(current);
        }
        else if (type === PieceType.Queen) {
            return new Queen().allowedMoves(current);
        }
        return new King().allowedMoves(current);
    }
    static createPiece(type) {
        switch (type) {
            case PieceType.King:
                return new King();
                break;
            case PieceType.Queen:
                return new Queen();
                break;
            case PieceType.Bishop:
                return new Bishop();
                break;
            case PieceType.Knight:
                return new Knight();
                break;
            case PieceType.Pawn:
                return new Pawn();
                break;
            case PieceType.Rook:
                return new Rook();
                break;
            default:
                return new King();
                break;
        }
    }
}
exports.Factory = Factory;
class King {
    constructor() {
        this._pieceType = PieceType.King;
    }
    get pieceType() {
        return this._pieceType;
    }
    allowedMoves(current) {
        const moves = [];
        return moves;
    }
}
exports.King = King;
class Queen {
    constructor() {
        this._pieceType = PieceType.Queen;
    }
    get pieceType() {
        return this._pieceType;
    }
    allowedMoves(current) {
        const moves = [];
        return moves;
    }
}
exports.Queen = Queen;
class Bishop {
    constructor() {
        this._pieceType = PieceType.Bishop;
    }
    get pieceType() {
        return this._pieceType;
    }
    allowedMoves(current) {
        const moves = [];
        return moves;
    }
}
exports.Bishop = Bishop;
class Rook {
    constructor() {
        this._pieceType = PieceType.Rook;
    }
    get pieceType() {
        return this._pieceType;
    }
    allowedMoves(current) {
        const moves = [];
        return moves;
    }
}
exports.Rook = Rook;
class Pawn {
    constructor() {
        this._pieceType = PieceType.Pawn;
    }
    get pieceType() {
        return this._pieceType;
    }
    allowedMoves(current) {
        const moves = [];
        return moves;
    }
}
exports.Pawn = Pawn;
class Knight {
    constructor() {
        this._pieceType = PieceType.Knight;
    }
    get pieceType() {
        return this._pieceType;
    }
    allowedMoves(current) {
        const moves = [];
        return moves;
    }
}
exports.Knight = Knight;
class Player {
    //    private pieces:Piece[] | null;
    constructor(username) {
        this.username = username;
    }
}
exports.Player = Player;
class Status {
}
exports.Status = Status;
class Turn {
    // private enemyKilledPiece: IPiece;
    // private allowedMoves: Cell[];
    constructor(currentPlayer, pieceType, currentPosition, nextMovePosition) {
        this.currentPlayer = currentPlayer;
        this.pieceType = pieceType;
        this.currentPosition = currentPosition;
        this.nextMovePosition = nextMovePosition;
    }
    allowedMoves() {
        const allowedMoves = Factory.allowedMovements(this.currentPosition, this.pieceType);
        return allowedMoves;
    }
    validateMove() {
        if (this.allowedMoves().find((move) => this.currentPosition))
            return true;
        return false;
    }
    move(board) {
        var _a;
        if (this.validateMove()) {
            const pieceType = (_a = this.currentPosition.piece) === null || _a === void 0 ? void 0 : _a.pieceType;
            const tmpPiece = pieceType != null ? Factory.createPiece(pieceType) : null;
            this.nextMovePosition.piece = this.currentPosition.piece;
            this.currentPosition.piece = null;
        }
    }
}
exports.Turn = Turn;
