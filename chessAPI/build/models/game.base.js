"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseGame = void 0;
const board_1 = __importDefault(require("./board"));
const enums_1 = require("./utils/enums");
const player_1 = __importDefault(require("./player"));
class BaseGame {
    constructor() {
        this._board = new board_1.default();
        this._playerBlack = new player_1.default();
        this._playerWhite = new player_1.default();
        this._gameStatus = enums_1.GameStatus.ReadyToStart;
        this._turns = new Array();
        this._currentTurn = enums_1.ColorSet.Black;
    }
    get currentTurn() {
        return this._currentTurn;
    }
    set currentTurn(value) {
        this._currentTurn = value;
    }
    get gameID() {
        var _a;
        return (_a = this._gameID) !== null && _a !== void 0 ? _a : undefined;
    }
    set gameID(value) {
        this._gameID = value;
    }
    get gameStatus() {
        return this._gameStatus;
    }
    set gameStatus(value) {
        this._gameStatus = value;
    }
    get board() {
        return this._board;
    }
    set board(value) {
        this._board = value;
    }
    get turns() {
        return this._turns;
    }
    set turns(value) {
        this._turns = value;
    }
    get playerWhite() {
        return this._playerWhite;
    }
    set playerWhite(value) {
        this._playerWhite = value;
    }
    get playerBlack() {
        return this._playerBlack;
    }
    set playerBlack(value) {
        this._playerBlack = value;
    }
}
exports.BaseGame = BaseGame;
//# sourceMappingURL=game.base.js.map