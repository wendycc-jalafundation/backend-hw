"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Cell {
    constructor(colorSet, row, col) {
        this._color = colorSet == 1 ? "black" : "white";
        this._row = row;
        this._column = col;
        this._piece = null;
    }
    get piece() {
        return this._piece;
    }
    set piece(value) {
        this._piece = value;
    }
    get color() {
        return this._color;
    }
    set color(value) {
        this._color = value;
    }
    get column() {
        return this._column;
    }
    set column(value) {
        this._column = value;
    }
    get row() {
        return this._row;
    }
    set row(value) {
        this._row = value;
    }
}
exports.default = Cell;
//# sourceMappingURL=cell.js.map