"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Knight = exports.Pawn = exports.Rook = exports.Bishop = exports.Queen = exports.King = void 0;
const cell_1 = __importDefault(require("./cell"));
const enums_1 = require("./enums");
class King {
    constructor(_colorSet) {
        this._colorSet = _colorSet;
        this._pieceType = enums_1.PieceType.King;
    }
    get colorSet() {
        var _a;
        return (_a = this._colorSet) !== null && _a !== void 0 ? _a : enums_1.ColorSet.Black;
    }
    get pieceType() {
        return this._pieceType;
    }
    allowedMoves(current) {
        const moves = [];
        return moves;
    }
}
exports.King = King;
class Queen {
    constructor(_colorSet) {
        this._colorSet = _colorSet;
        this._pieceType = enums_1.PieceType.Queen;
    }
    get colorSet() {
        var _a;
        return (_a = this._colorSet) !== null && _a !== void 0 ? _a : enums_1.ColorSet.Black;
    }
    get pieceType() {
        return this._pieceType;
    }
    allowedMoves(current) {
        const moves = [];
        return moves;
    }
}
exports.Queen = Queen;
class Bishop {
    constructor(_colorSet) {
        this._colorSet = _colorSet;
        this._pieceType = enums_1.PieceType.Bishop;
    }
    get colorSet() {
        var _a;
        return (_a = this._colorSet) !== null && _a !== void 0 ? _a : enums_1.ColorSet.Black;
    }
    get pieceType() {
        return this._pieceType;
    }
    allowedMoves(current) {
        const moves = [];
        return moves;
    }
}
exports.Bishop = Bishop;
class Rook {
    constructor(_colorSet) {
        this._colorSet = _colorSet;
        this._pieceType = enums_1.PieceType.Rook;
    }
    get colorSet() {
        var _a;
        return (_a = this._colorSet) !== null && _a !== void 0 ? _a : enums_1.ColorSet.Black;
    }
    get pieceType() {
        return this._pieceType;
    }
    allowedMoves(current) {
        const moves = [];
        return moves;
    }
}
exports.Rook = Rook;
class Pawn {
    constructor(_colorSet) {
        this._colorSet = _colorSet;
        this._pieceType = enums_1.PieceType.Pawn;
    }
    get colorSet() {
        var _a;
        return (_a = this._colorSet) !== null && _a !== void 0 ? _a : enums_1.ColorSet.Black;
    }
    get pieceType() {
        return this._pieceType;
    }
    allowedMoves(current) {
        const moves = [];
        const newCell = new cell_1.default(1, 3, 2);
        moves.push(newCell);
        return moves;
    }
}
exports.Pawn = Pawn;
class Knight {
    constructor(_colorSet) {
        this._colorSet = _colorSet;
        this._pieceType = enums_1.PieceType.Knight;
    }
    get colorSet() {
        var _a;
        return (_a = this._colorSet) !== null && _a !== void 0 ? _a : enums_1.ColorSet.Black;
    }
    get pieceType() {
        return this._pieceType;
    }
    allowedMoves(current) {
        const moves = [];
        return moves;
    }
}
exports.Knight = Knight;
//# sourceMappingURL=piece.js.map