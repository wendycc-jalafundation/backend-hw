"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MongoDBRepository = void 0;
const bson_1 = require("bson");
const mongodb_1 = require("mongodb");
const board_1 = __importDefault(require("../models/board"));
const cell_1 = __importDefault(require("../models/cell"));
const enums_1 = require("../models/utils/enums");
const game_1 = __importDefault(require("../models/game"));
const piece_factory_1 = __importDefault(require("../models/utils/piece.factory"));
const turn_1 = __importDefault(require("../models/turn"));
const database_service_1 = require("../services/database.service");
const player_1 = __importDefault(require("../models/player"));
class MongoDBRepository {
    findOne(id) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield ((_a = database_service_1.collections.games) === null || _a === void 0 ? void 0 : _a.findOne({ _id: id }));
            const deserializedGameObject = this.deserializeGameObject(JSON.stringify(result));
            return deserializedGameObject;
        });
    }
    create(game) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            const newGameID = new mongodb_1.ObjectId();
            game.gameID = newGameID + "";
            const result = yield ((_a = database_service_1.collections.games) === null || _a === void 0 ? void 0 : _a.insertOne(Object.assign(Object.assign({}, game), { _id: newGameID })));
            return !!(result === null || result === void 0 ? void 0 : result.insertedId);
        });
    }
    findAll() {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            debugger;
            const result = yield ((_a = database_service_1.collections.games) === null || _a === void 0 ? void 0 : _a.find({}).toArray());
            let games = new Array();
            if ((result === null || result === void 0 ? void 0 : result.length) !== undefined && result.length > 0)
                for (let index = 0; index < result.length; index++) {
                    const resultToJson = JSON.stringify(result[index]);
                    const resultToGame = this.deserializeGameObject(resultToJson);
                    games.push(resultToGame);
                }
            return games;
        });
    }
    update(id, game) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            const objectId = new bson_1.ObjectID(id);
            const query = { _gameID: id };
            const replacement = {
                _board: game.board,
                _gameStatus: game.gameStatus,
                _turns: game.turns,
                _currentTurn: game.currentTurn
            };
            const result = yield ((_a = database_service_1.collections.games) === null || _a === void 0 ? void 0 : _a.findOneAndUpdate(query, {
                $set: replacement,
            }));
            return yield this.findOne(objectId);
        });
    }
    updateBoard(id, board) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            const objid = new bson_1.ObjectID(id);
            const objboar = board;
            const query = { _gameID: id };
            const replacement = {
                _board: board,
            };
            const result = yield ((_a = database_service_1.collections.games) === null || _a === void 0 ? void 0 : _a.findOneAndUpdate(query, {
                $set: replacement,
            }));
            return yield this.findOne(objid);
        });
    }
    updateTurns(id, turns) {
        throw new Error("Method not implemented.");
    }
    deserializeGameObject(jsonString) {
        debugger;
        const game = JSON.parse(jsonString);
        const newGame = new game_1.default();
        const board = new board_1.default();
        let cells = new Array();
        cells = game._board._cells.map((cell) => {
            const colorSet = cell._color == "black" ? 1 : 0;
            const newCell = new cell_1.default(colorSet, cell._row, cell._column);
            let newPiece;
            if (cell._piece != null) {
                newPiece = piece_factory_1.default.createPiece(cell._piece._pieceType, cell._piece._colorSet);
            }
            else {
                newPiece = null;
            }
            newCell.piece = newPiece;
            return newCell;
        });
        board.cells = cells;
        newGame.board = board;
        newGame.gameStatus = game._gameStatus;
        newGame.currentTurn = game._currentTurn;
        newGame.gameID = game._gameID;
        const playerBlack = new player_1.default();
        playerBlack.username = game._playerBlack._username;
        playerBlack.colorSet = enums_1.ColorSet.Black;
        const playerWhite = new player_1.default();
        playerWhite.username = game._playerWhite._username;
        playerBlack.colorSet = enums_1.ColorSet.White;
        newGame.playerBlack = playerBlack;
        newGame.playerWhite = playerWhite;
        let turns = new Array();
        turns = game._turns.map((turn) => {
            let cells = new Array();
            cells = game._board._cells.map((cell) => {
                const colorSet = cell._color == "black" ? 1 : 0;
                const newCell = new cell_1.default(colorSet, cell._row, cell._column);
                let newPiece;
                if (cell._piece != null) {
                    newPiece = piece_factory_1.default.createPiece(cell._piece._pieceType, cell._piece._colorSet);
                }
                else {
                    newPiece = null;
                }
                newCell.piece = newPiece;
                return newCell;
            });
            const board = new board_1.default();
            board.cells = cells;
            let piece;
            if (turn._killedPiece != null) {
                piece = piece_factory_1.default.createPiece(turn._killedPiece._pieceType, turn._killedPiece._colorSet);
            }
            else {
                piece = null;
            }
            const newTurn = new turn_1.default(turn._player, board, piece);
            return newTurn;
        });
        board.cells = cells;
        board.size = game._board._size;
        newGame.board = board;
        newGame.gameStatus = game._gameStatus;
        newGame.gameID = game._gameID;
        newGame.turns = turns;
        return newGame;
    }
}
exports.MongoDBRepository = MongoDBRepository;
//# sourceMappingURL=mongodb.repository.js.map