"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const app_1 = __importDefault(require("./app"));
const game_controller_1 = __importDefault(require("./controllers/game.controller"));
const PORT = process.env.PORT || 8080;
const app = new app_1.default([
    new game_controller_1.default()
], PORT);
app.listen();
//# sourceMappingURL=index.js.map