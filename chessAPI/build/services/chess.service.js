"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Chess = void 0;
const enums_1 = require("../models/utils/enums");
const piece_factory_1 = __importDefault(require("../models/utils/piece.factory"));
class Chess {
    constructor(game) {
        this.game = game;
    }
    executeTurn(colorSet, originCoordinates, targetCoordinates) {
        debugger;
        console.log("inside chess", this.game.board);
        const movementCells = this.getMovementCells(originCoordinates, targetCoordinates);
        const origin = movementCells.origin;
        const target = movementCells.target;
        const isValid = this.moveValidation(colorSet, origin, target);
        let moveType = enums_1.MoveType.Undefined;
        if (isValid) {
            moveType = this.clasifyMove(target);
        }
        let killedPiece = null;
        if (moveType === enums_1.MoveType.Normal) {
            this.executeNormalMove(origin, target);
        }
        else if (moveType === enums_1.MoveType.Kill) {
            killedPiece = this.executeKillMove(origin, target);
        }
        return killedPiece;
    }
    changeBoardState(newBoardState, gameStatus, game) {
        game.board = newBoardState;
        game.gameStatus = gameStatus;
    }
    clasifyMove(target) {
        if (target.piece === null) {
            return enums_1.MoveType.Normal;
        }
        return enums_1.MoveType.Kill;
    }
    executeNormalMove(origin, target) {
        var _a, _b;
        target.piece = piece_factory_1.default.createPiece((_a = origin.piece) === null || _a === void 0 ? void 0 : _a.pieceType, (_b = origin.piece) === null || _b === void 0 ? void 0 : _b.colorSet);
        origin.piece = null;
    }
    executeKillMove(origin, target) {
        var _a, _b, _c, _d;
        const killedPiece = piece_factory_1.default.createPiece((_a = target.piece) === null || _a === void 0 ? void 0 : _a.pieceType, (_b = target.piece) === null || _b === void 0 ? void 0 : _b.colorSet);
        target.piece = piece_factory_1.default.createPiece((_c = origin.piece) === null || _c === void 0 ? void 0 : _c.pieceType, (_d = origin.piece) === null || _d === void 0 ? void 0 : _d.colorSet);
        origin.piece = null;
        return killedPiece;
    }
    generateCoordinates(moveCoordinates) {
        const originCoordinates = moveCoordinates.split("-");
        const row = +originCoordinates[0];
        const column = +originCoordinates[1];
        return {
            row: row,
            column: column,
        };
    }
    getMovementCells(originCoordinates, targetCoordinates) {
        console.log("inside chess/getmovements", this.game.board);
        const getOriginCell = this.generateCoordinates(originCoordinates);
        const getTargetCell = this.generateCoordinates(targetCoordinates);
        const originCell = this.game.getCell(getOriginCell.row, getOriginCell.column);
        const targetCell = this.game.getCell(getTargetCell.row, getTargetCell.column);
        return {
            origin: originCell,
            target: targetCell,
        };
    }
    moveValidation(colorSet, origin, target) {
        const originCell = this.game.getCell(origin.row, origin.column);
        const targetCell = this.game.getCell(target.row, target.column);
        const allowedMoves = piece_factory_1.default.allowedMovements(originCell, colorSet, this.game.board);
        const validateMove = allowedMoves.find((cell) => cell.row === targetCell.row && cell.column === targetCell.column);
        // TODO: validate if player is white or black
        if (validateMove)
            return true;
        return false;
    }
    moveValidationforGameService(colorSet, originCoordinates, targetCoordinates) {
        let isValid = true;
        console.log("inside chess", this.game.board);
        const movementCells = this.getMovementCells(originCoordinates, targetCoordinates);
        const origin = movementCells.origin;
        const target = movementCells.target;
        isValid = this.moveValidation(colorSet, origin, target);
        return isValid;
    }
}
exports.Chess = Chess;
//# sourceMappingURL=chess.service.js.map