"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const bson_1 = require("bson");
const enums_1 = require("../models/utils/enums");
const game_1 = __importDefault(require("../models/game"));
const turn_1 = __importDefault(require("../models/turn"));
const chess_service_1 = require("./chess.service");
class GameService {
    constructor(gameRepository) {
        this.gameRepository = gameRepository;
    }
    findAll() {
        return __awaiter(this, void 0, void 0, function* () {
            return this.gameRepository.findAll();
        });
    }
    newGame(playerBlack, playerWhite, starts) {
        return __awaiter(this, void 0, void 0, function* () {
            const newGame = new game_1.default();
            newGame.playerBlack.username = playerBlack;
            newGame.playerWhite.username = playerWhite;
            newGame.currentTurn = starts;
            newGame.initializeBoard();
            return this.gameRepository.create(newGame);
        });
    }
    findGame(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const objecctId = new bson_1.ObjectID(id);
            return this.gameRepository.findOne(objecctId);
        });
    }
    executeNewTurnWhite(gameID, originCoordinates, targetCoordinates) {
        return __awaiter(this, void 0, void 0, function* () {
            debugger;
            const game = yield this.findGame(gameID);
            const chess = new chess_service_1.Chess(game);
            const pieceKilled = chess.executeTurn(enums_1.ColorSet.White, originCoordinates, targetCoordinates);
            const newTurn = new turn_1.default(enums_1.ColorSet.White, game.board, pieceKilled);
            game.turns.push(newTurn);
            game.gameStatus = enums_1.GameStatus.Playing;
            game.currentTurn = enums_1.ColorSet.Black;
            const updatedGame = yield this.gameRepository.update(gameID, game);
            return updatedGame;
        });
    }
    executeNewTurnBlack(gameID, originCoordinates, targetCoordinates) {
        return __awaiter(this, void 0, void 0, function* () {
            debugger;
            const game = yield this.findGame(gameID);
            const chess = new chess_service_1.Chess(game);
            const pieceKilled = chess.executeTurn(enums_1.ColorSet.Black, originCoordinates, targetCoordinates);
            const newTurn = new turn_1.default(enums_1.ColorSet.Black, game.board, pieceKilled);
            game.turns.push(newTurn);
            game.gameStatus = enums_1.GameStatus.Playing;
            game.currentTurn = enums_1.ColorSet.White;
            const updatedGame = yield this.gameRepository.update(gameID, game);
            return updatedGame;
        });
    }
    validateTurnWhite(gameID) {
        return __awaiter(this, void 0, void 0, function* () {
            debugger;
            const game = yield this.findGame(gameID);
            if (game.currentTurn != enums_1.ColorSet.White)
                return false;
            return true;
        });
    }
    validateTurnBlack(gameID) {
        return __awaiter(this, void 0, void 0, function* () {
            debugger;
            const game = yield this.findGame(gameID);
            if (game.currentTurn != enums_1.ColorSet.Black)
                return false;
            return true;
        });
    }
    validateMove(gameID, originCoordinates, targetCoordinates) {
        return __awaiter(this, void 0, void 0, function* () {
            const game = yield this.findGame(gameID);
            const chess = new chess_service_1.Chess(game);
            const isValid = chess.moveValidationforGameService(0, originCoordinates, targetCoordinates);
            return isValid;
        });
    }
}
exports.default = GameService;
//# sourceMappingURL=game.services.js.map