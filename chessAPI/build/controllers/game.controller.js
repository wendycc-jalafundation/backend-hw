"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const mongodb_repository_1 = require("../repositories/mongodb.repository");
const game_services_1 = __importDefault(require("../services/game.services"));
class GameController {
    constructor() {
        this.path = "/games";
        this.router = express_1.default.Router();
        const gamesRepository = new mongodb_repository_1.MongoDBRepository();
        this.gamesService = new game_services_1.default(gamesRepository);
        this.initializeRoutes();
    }
    initializeRoutes() {
        this.router.get(this.path, this.getGames.bind(this));
        this.router.post(this.path, this.newGame.bind(this));
        this.router.get(this.path + "/find", this.getGame.bind(this));
        this.router.post(this.path + "/turn/white", this.newTurnWhite.bind(this));
        this.router.post(this.path + "/turn/black", this.newTurnBlack.bind(this));
    }
    getGame(_req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                debugger;
                const game = yield this.gamesService.findGame(_req.body.gameid);
                res.status(200).send(game);
            }
            catch (e) {
                res.status(500);
            }
        });
    }
    getGames(_req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                debugger;
                const games = yield this.gamesService.findAll();
                res.status(200).send(games);
            }
            catch (e) {
                res.status(500);
            }
        });
    }
    newGame(_req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const result = yield this.gamesService.newGame(_req.body.playerBlack, _req.body.playerWhite, _req.body.starts);
                result
                    ? res.status(201).send(`Successfully created a new game`)
                    : res.status(500).send("Failed to create a new game.");
            }
            catch (error) {
                console.error(error);
                res.status(400).send(error);
            }
        });
    }
    newTurnWhite(_req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                debugger;
                const validation = yield this.gamesService.validateTurnWhite(_req.body.gameID);
                if (!validation) {
                    res.status(500).send("It not your turn");
                }
                else {
                    const moveValidation = yield this.gamesService.validateMove(_req.body.gameID, _req.body.originPosition, _req.body.targetPosition);
                    if (moveValidation) {
                        const result = yield this.gamesService.executeNewTurnWhite(_req.body.gameID, _req.body.originPosition, _req.body.targetPosition);
                        result
                            ? res.status(201).send("Piece moved")
                            : res.status(500).send("Error");
                    }
                    else {
                        res.status(500).send("Invalid move");
                    }
                }
            }
            catch (error) {
                console.error(error);
                res.status(400).send(error);
            }
        });
    }
    newTurnBlack(_req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                debugger;
                const validation = yield this.gamesService.validateTurnBlack(_req.body.gameID);
                if (!validation) {
                    res.status(500).send("It not your turn");
                }
                else {
                    const moveValidation = yield this.gamesService.validateMove(_req.body.gameID, _req.body.originPosition, _req.body.targetPosition);
                    if (moveValidation) {
                        const result = yield this.gamesService.executeNewTurnWhite(_req.body.gameID, _req.body.originPosition, _req.body.targetPosition);
                        result
                            ? res.status(201).send("Piece moved")
                            : res.status(500).send("Error");
                    }
                    else {
                        res.status(500).send("Invalid move");
                    }
                }
            }
            catch (error) {
                console.error(error);
                res.status(400).send(error);
            }
        });
    }
}
exports.default = GameController;
//# sourceMappingURL=game.controller.js.map