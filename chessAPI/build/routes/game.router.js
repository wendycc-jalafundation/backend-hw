"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GamesRouter = void 0;
const express_1 = require("express");
class GamesRouter {
    //gamesController: GameController;
    constructor() {
        this.router = (0, express_1.Router)();
        //this.gamesController= new GameController();
        this.init();
    }
    init() {
        // this.router.get("/", this.gamesController.getGames);
        // this.router.post("/", this.gamesController.newGame);
    }
}
exports.GamesRouter = GamesRouter;
const gamesRoutes = new GamesRouter();
gamesRoutes.init();
exports.default = gamesRoutes.router;
