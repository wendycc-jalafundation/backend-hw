# Homework
## 1. Collections
Link to [`My solution`](/collectionHomework/collections.md) and the examples

## 2. Dynamic modules
Link to the [`solution`](/modules) (modules and unit testing)

## 3. Bigint
Link to the [`solution`](/bigint)
## 4. Open/Close

Link to the [`solution`](/openClose)
## 5. Single Responsability

Link to the [`solutions`](/singleResponsability)
## 4. Final - Chess API
Link to the [`Chess API Project`](/chessAPI)

## 5. RestAPI presentation
[Link to the REST API presentation](/RestAPI-Presentation)

## 6. RabbitMQ - Exchange type: Header
[Link to the Header exchange example and presentation](/rabbit-exchange-header)

## 7. Attendance Project
[Link to the project](/attendance-project)