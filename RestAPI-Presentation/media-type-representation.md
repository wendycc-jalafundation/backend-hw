---
marp: true


---

# **Media Type Representation**

---

# WRML
```json

Content-Type:   application/json

```
```json
Content-Type:   application/wrml;
                format="http://api.formats.wrml.org/application/json";
                schema="http://api.schemas.wrml.org/soccer/Player";
```
---

### Rule: A consistent form should be used to represent media type **formats**
```json
Content-Type:   application/wrml;
                format="http://api.formats.wrml.org/application/json";
                schema="http://api.schemas.wrml.org/common/Format";
```
---
### Rule: A consistent form should be used to represent media type **formats**
```json
{
    "mediaType" : Text <constrained by media type syntax>,
    "links" : {
    "home" : Link <form constrained by the Link schema>,
    "rfc" : Link <form constrained by the Link schema>
},
    "serialize" : {
    "links" : {
        <Set of Link schema-constrained forms>
    }
},
    "deserialize" : {
        "links" : {
            <Set of Link schema-constrained forms>
        }
    }
}
```
---
### Rule: A consistent form should be used to represent media type **formats**
```json
# Request
GET /application/json HTTP/1.1
Host: api.formats.wrml.org
# Response
HTTP/1.1 200 OK
Content-Type: application/wrml;
format="http://api.formats.wrml.org/application/json";
schema="http://api.schemas.wrml.org/common/Format"
{
    "mediaType" : "application/json",
    "links" : {
        "self" : {
        "href" : "http://api.formats.wrml.org/application/json",
        "rel" : "http://api.relations.wrml.org/common/self"
    },
    "home" : {
        "href" : "http://www.json.org",
        "rel" : "http://api.relations.wrml.org/common/home"
        },
    "rfc" : {
        "href" : "http://www.rfc-editor.org/rfc/rfc4627.txt",
        "rel" : "http://api.relations.wrml.org/format/rfc"
        }
    },
    "serialize" : {
        "links" : {
            "java" : {
                "href" : "http://api.formats.wrml.org/application/json/serializers/java",
                "rel" : "http://api.relations.wrml.org/format/serialize/java"
            },
        }
    },
    "deserialize" : {
        "links" : {
            "java" : {
                "href" : "http://api.formats.wrml.org/application/json/deserializers/java",
                "rel" : "http://api.relations.wrml.org/format/deserialize/java"
            },

        }
    }
}
```
---
### Rule: A consistent form should be used to represent media type **schemas**

```json
application/wrml;
    format="http://api.formats.wrml.org/application/json";
    schema="http://api.schemas.wrml.org/common/Schema"
```
---
### Rule: A consistent form should be used to represent media type **schemas**
```json
{
    "name" : Text <constrained to be mixed uppercase>,
    "version" : Integer,
    "extends" : Array <constrained to contain (schema) URI text elements>,
    "fields" : {
        <Set of Field schema-constrained forms>
    },
    "stateFacts" : Array <constrained to contain mixed uppercase text elements>,
    "linkFormulas" : {
        <Set of LinkFormula schema-constrained forms>
    }
},
"description" : Text
}
```
---
### Rule: A consistent form should be used to represent media type **schemas**
#### Field representatio
Types: “Boolean,”
“Choice,” “DateTime,” “Double,” “Integer,” “List,” “Schema,” or “Text.”
```json
application/wrml;
    format="http://api.formats.wrml.org/application/json";
    schema="http://api.schemas.wrml.org/common/Field"
```
```json
{
    "type" : Text <constrained to be one of the primitive field types>,
    "defaultValue" : <a type-specific value>,
    "readOnly" : Boolean,
    "required" : Boolean,
    "hidden" : Boolean,
    "constraints" : Array <constrained to contain (constraint) URI text elements>,
    "description" : Text
}
```
---
### Rule: A consistent form should be used to represent media type **schemas**
#### Link Representation
```json
application/wrml;
    format="http://api.formats.wrml.org/application/json";
    schema="http://api.schemas.wrml.org/common/Constraint"
```
```json
{
    "rel" : Text <constrained by URI syntax>,
    "condition" : Text <constrained to be a state fact-based Boolean expression>
}
```
---
#### Other schemas representation
![diagram](diagram.png)