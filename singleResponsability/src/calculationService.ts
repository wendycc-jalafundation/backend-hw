import Invoice from "./invoice";
export class CalculationService {
  total(invoice: Invoice) {
    console.log(
      `${invoice.book.getName()} Invoice quantity: ${invoice.quantity}`
    );
    console.log(`Discount Rate: ${invoice.discountRate}`);
    console.log(`Tax Rate: ${invoice.taxRate}`);
    console.log(`TOTAL: ${invoice.total}`);
  }
}