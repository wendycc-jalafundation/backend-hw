import Book from "./book";
export default class Invoice {
  private _total: number;
  constructor(
    private _book: Book,
    private _quantity: number,
    private _discountRate: number,
    private _taxRate: number
  ) {}
  
  get total(){ return this._total; }

  get book(){ return this._book; } 

  get quantity(){ return this._quantity }

  get discountRate(){ return this._discountRate }

  get taxRate(){ return this._taxRate }
  
  
}