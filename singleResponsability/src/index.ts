import Book from "./book";
import Invoice from "./invoice";
import { CalculationService } from "./calculationService";
import { PrintingService } from "./printService";
import { SaveService } from "./saveService";


const book = new Book("Clen Code", "Bob", 1995, 49, "SD-456-ASD");
const invoice = new Invoice(book, 1, 50, 0.14);

const calcService = new CalculationService();
const printService = new PrintingService();
const saveService =  new SaveService();

calcService.total(invoice);
printService.print(invoice);
saveService.toFile(invoice);


// using static:
// CalculatioService.total(invoice);
// PrintingService.print(invoice);
// SaveService.toFile(invoice);